package sinfo.api.controller;

import org.springframework.web.bind.annotation.*;
import sinfo.api.request.DetectionRequest;
import sinfo.api.request.RolesRequest;
import sinfo.api.service.RolesService;

import sinfo.common.Classinstance.Detectionre;
import sinfo.common.Classinstance.Roles;
import sinfo.common.model.page.PageRequestModel;
import sinfo.common.model.page.Pager;
import sinfo.model.entity.RolesEntity;
import sinfo.util.message.BaseResponse;
import sinfo.util.util.DateUtil;
import sinfo.util.util.ResponseUtil;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("roles")
public class RolesController {

    @Resource
    private RolesService rolesService;

    @PostMapping("addroles")

    public BaseResponse addroles(@RequestBody  RolesRequest request) throws Exception {
        request.validate();

        Roles roles = new Roles();
        roles.setRole_name(request.getRole_name());
        roles.setRole_remark(request.getRole_remark());
        roles.setRole_status(1);
        roles.setRole_permissions(request.getRole_permissions());
        roles.setDelete_flag(0);
        DateUtil  datautil=new DateUtil();
        Date dats= DateUtil.convertStrToDate(datautil.getCurrDateStr("yyyy-MM-dd HH:mm:ss"),
                DateUtil.DEFAULT_DATE_TIME_FORMAT);;
        roles.setCreate_date(dats);
        roles.setUpdate_date(dats);

        rolesService.addRole(roles);
        return ResponseUtil.success();


    }



    @PostMapping("uproles")

    public BaseResponse uproles(@RequestBody  RolesRequest request) throws Exception {
        request.validate();

        Roles roles = new Roles();
        roles.setRole_name(request.getRole_name());
        roles.setRole_remark(request.getRole_remark());
        roles.setRole_status(1);
        roles.setRole_permissions(request.getRole_permissions());
        roles.setDelete_flag(0);
        DateUtil  datautil=new DateUtil();
        Date dats= DateUtil.convertStrToDate(datautil.getCurrDateStr("yyyy-MM-dd HH:mm:ss"),
                DateUtil.DEFAULT_DATE_TIME_FORMAT);
        roles.setUpdate_date(dats);


        rolesService.updateRole(roles);

        return ResponseUtil.success();

    }

    

    @PostMapping("delroles")
    public BaseResponse delroles(@RequestBody RolesRequest request) throws Exception {
        //  request.validate();
        Roles roles = new Roles();

        roles.setId(request.getId());

        rolesService.delRole(roles);

        return ResponseUtil.success();
    }


    @GetMapping("getRolesInfo")
    public BaseResponse getRoleInfo(RolesRequest request) throws Exception {

        List<RolesEntity> list = rolesService.getRolesInfo(request.getRole_name());

        return ResponseUtil.data(list);

    }



    @GetMapping("getRoleslist")
    public BaseResponse getRolesList(PageRequestModel pageRequest) throws Exception {
        pageRequest.validate();
        Pager<RolesEntity, Object> pager = rolesService.getList(Integer.parseInt(pageRequest.getPage()), Integer.parseInt(pageRequest.getPage()));
        return ResponseUtil.data(pager);
    }



















}
