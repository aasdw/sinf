package sinfo.api.controller;

import org.springframework.web.bind.annotation.*;
import sinfo.api.request.RoleQueryRequest;
import sinfo.api.request.RoleRequest;
import sinfo.api.service.RoleService;
import sinfo.common.annotation.Authorization;
import sinfo.common.model.page.Pager;
import sinfo.model.vo.RoleVo;
import sinfo.util.message.BaseResponse;
import sinfo.util.util.ResponseUtil;

import javax.annotation.Resource;
import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping("role")
public class RoleController {

    @Resource
    private RoleService roleService;

    @PostMapping("addOrUpdate")
    @Authorization
    public BaseResponse addOrUpdate(@RequestBody RoleRequest request) throws Exception {
        request.validate();
        roleService.addOrUpdate(request);
        return ResponseUtil.success();
    }

    @GetMapping("query-role-info")
    @Authorization
    public BaseResponse queryRoleInfo(Integer id) throws Exception {
        if (Objects.isNull(id)) {
            throw new IllegalArgumentException("id不能为空");
        }
        return ResponseUtil.data(roleService.queryRoleInfo(id));
    }

    @PostMapping("delete")
    @Authorization
    public BaseResponse delete(@RequestBody Map<String, Object> param) throws Exception {
        if (!param.containsKey("id")) {
            throw new IllegalArgumentException("id不能为空");
        }
        roleService.delete(Integer.parseInt(param.get("id").toString()));
        return ResponseUtil.success();
    }

    @GetMapping("list")
    @Authorization
    public BaseResponse getList(RoleQueryRequest request) throws Exception {
        request.validate();
        Pager<RoleVo, Object> pager = roleService.getList(request);
        return ResponseUtil.data(pager);
    }

}
