package sinfo.api.controller;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sinfo.util.message.BaseResponse;
import sinfo.util.util.RandomUtil;
import sinfo.util.util.ResponseUtil;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("test")
public class TestController {

    @GetMapping("test")
    public BaseResponse getWebToken(String code, HttpServletRequest request) throws Exception {
        if (StringUtils.isEmpty(code)) {
            throw new IllegalArgumentException("code不能为空");
        }
        JSONObject json = new JSONObject();
        json.put("test", RandomUtil.getUUID());
        return ResponseUtil.data(json);
    }
}