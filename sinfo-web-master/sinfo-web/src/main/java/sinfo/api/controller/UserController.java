package sinfo.api.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import sinfo.api.request.UserRequest;
import sinfo.api.service.RolesService;
import sinfo.api.service.UserService;
import sinfo.common.constant.Constants;
import sinfo.common.model.page.PageRequestModel;
import sinfo.common.model.page.Pager;
import sinfo.model.entity.RolesEntity;
import sinfo.model.entity.UserEntity;
import sinfo.util.cache.redis.RedisUtil;
import sinfo.util.message.BaseResponse;
import sinfo.util.util.ResponseUtil;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("user")
public class UserController {

    @Resource
    private UserService userService;


    @Resource
    private RedisUtil redisUtil;

    @PostMapping("add-user")
    public BaseResponse addUser(@RequestBody UserRequest request) throws Exception {
        request.validate();
        userService.addUser(request);
        return ResponseUtil.success();
    }


    @PostMapping("update-user")
    public BaseResponse updateUser(@RequestBody UserRequest request) throws Exception {
        request.validate();
        if (StringUtils.isEmpty(request.getMemberId())) {
            throw new IllegalArgumentException("memberId不能为空");
        }
        userService.updateUser(request);
        return ResponseUtil.success();
    }






    @GetMapping("query-userInfo")
    public BaseResponse queryUserInfo(String memberId) throws Exception {
        if (StringUtils.isEmpty(memberId)) {
            throw new IllegalArgumentException("memberId不能为空");
        }
        UserEntity userEntity = userService.queryUserInfoById(memberId);
        //redisUtil.set("111","222");
        //String str = redisUtil.get("123");
        //redisUtil.del("111", "123", "png", "gif");
        redisUtil.expire("111", Constants.REDIS_KEY_CLIENT_TOKEN_EXPIRE);
        return ResponseUtil.data("222");
    }

    @GetMapping("getUserInfo")
    public BaseResponse getUserInfo(String memberId) throws Exception {
        List<UserEntity> list = userService.getUserInfo(memberId);
        return ResponseUtil.data(list);
    }

    @GetMapping("list")
    public BaseResponse getList(PageRequestModel pageRequest) throws Exception {
        pageRequest.validate();
        Pager<UserEntity, Object> pager = userService.getList(Integer.parseInt(pageRequest.getPage()), Integer.parseInt(pageRequest.getPage()));
        return ResponseUtil.data(pager);
    }





    /**
     * 密码加密demo
     * 注：前端需要将密码加密(MD5)传给后端
     *
     * @param args
     */
    public static void main(String[] args) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        // 加密
        System.out.println(bCryptPasswordEncoder.encode("qwe123456"));
        // 验证密码是否匹配
        boolean bMatch = bCryptPasswordEncoder.matches("qwe123456", "$2a$10$L9JGVYpe7eg4NOUY0W3pneBtG.YtdM7W9eef0z3ONhTEu6HkPQ0zW");
        System.out.println(bMatch);
    }



}
