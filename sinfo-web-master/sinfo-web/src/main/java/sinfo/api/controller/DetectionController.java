package sinfo.api.controller;


import org.springframework.web.bind.annotation.*;
import sinfo.api.request.DetectionRequest;

import sinfo.api.service.DetectionreService;

import sinfo.common.Classinstance.Detectionre;
import sinfo.common.model.page.PageRequestModel;
import sinfo.common.model.page.Pager;
import sinfo.model.entity.DetectionreEntity;

import sinfo.util.message.BaseResponse;
import sinfo.util.util.DateUtil;
import sinfo.util.util.ResponseUtil;

import javax.annotation.Resource;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("detectionre")
public class DetectionController {

    @Resource
    private DetectionreService detectionService;



    @PostMapping("adddetection")
    public BaseResponse adddetection(@RequestBody  DetectionRequest request) throws Exception {
        request.validate();
        Detectionre detec=new Detectionre();

        detec.setType(request.getType());
        detec.setScore(request.getScore());
        detec.setLevel(request.getLevel());
        detec.setContent(request.getContent());
        detec.setCreator(request.getCreator());
        detec.setDelete_flag(0);

        DateUtil datautil=new DateUtil();
        Date dats= DateUtil.convertStrToDate(datautil.getCurrDateStr("yyyy-MM-dd HH:mm:ss"),
                DateUtil.DEFAULT_DATE_TIME_FORMAT);

        detec.setCreate_date(dats);
        detec.setUpdate_date(dats);

        detectionService.addDetection(detec);
        return ResponseUtil.success();
    }





    @PostMapping("deldetection")
    public BaseResponse deldetection(@RequestBody  DetectionRequest request) throws Exception {
        //  request.validate();
        Detectionre detec=new Detectionre();

        detec.setId(request.getId());

        detectionService.delDetection(detec);

        return ResponseUtil.success();
    }




    @PostMapping("updetection")
    public BaseResponse updetection(@RequestBody  DetectionRequest request) throws Exception {
        //  request.validate();
        Detectionre detec=new Detectionre();

        detec.setId(request.getId());
        detec.setScore(request.getScore());
        detec.setType(request.getType());
        detec.setLevel(request.getLevel());
        detec.setContent(request.getContent());
        detec.setCreator(request.getCreator());
        detec.setDelete_flag(0);
        DateUtil datautil=new DateUtil();
        Date dats= DateUtil.convertStrToDate(datautil.getCurrDateStr("yyyy-MM-dd HH:mm:ss"),
                DateUtil.DEFAULT_DATE_TIME_FORMAT);;
        detec.setUpdate_date(dats);

        detectionService.upDetection(detec);

        return ResponseUtil.success();
    }




    @GetMapping("getdetection")
    public BaseResponse getdetection(DetectionRequest request) throws Exception {

        Detectionre detec=new Detectionre();

        detec.setCreator(request.getCreator());

        List<Detectionre> list;
        list = detectionService.getDetection(detec);

        return ResponseUtil.data(list);

    }






    @GetMapping("getdetectionInfo")
    public BaseResponse getdetectionInfo(DetectionRequest request) throws Exception {

        List<DetectionreEntity> list = detectionService.getDetectionInfo(request.getCreator());

        return ResponseUtil.data(list);

     }


///page  //pageSize
    @GetMapping("getdetectionlist")
    public BaseResponse getdetectionList(PageRequestModel pageRequest) throws Exception {
        pageRequest.validate();
        Pager<DetectionreEntity, Object> pager = detectionService.getList(Integer.parseInt(pageRequest.getPage()), Integer.parseInt(pageRequest.getPage()));
        return ResponseUtil.data(pager);
    }












}
