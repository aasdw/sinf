package sinfo.api.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sinfo.api.request.CustomerQueryRequest;
import sinfo.api.service.CustomerService;
import sinfo.common.annotation.Authorization;
import sinfo.common.model.page.Pager;
import sinfo.model.vo.CustomerVo;
import sinfo.util.message.BaseResponse;
import sinfo.util.util.ResponseUtil;

import javax.annotation.Resource;

@RestController
@RequestMapping("customer")
public class CustomerController {

    @Resource
    private CustomerService customerService;

    @GetMapping("list")
    @Authorization
    public BaseResponse getList(CustomerQueryRequest request) throws Exception {
        request.validate();
        Pager<CustomerVo, Object> pager = customerService.getList(request);
        return ResponseUtil.data(pager);
    }

}
