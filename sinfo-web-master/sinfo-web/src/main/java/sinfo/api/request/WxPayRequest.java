package sinfo.api.request;

import org.apache.commons.lang3.StringUtils;
import sinfo.common.request.IHttpRequest;

import java.math.BigDecimal;
import java.util.Objects;

public class WxPayRequest implements IHttpRequest {
    private String openId;
    private String buyerId;
    private BigDecimal orderMoney;
    private BigDecimal payMoney;
    private String merchantId;
    private String storeCollectionId;

    @Override
    public void validate() throws IllegalArgumentException {
        if (StringUtils.isEmpty(getOpenId()) && StringUtils.isEmpty(getBuyerId())) {
            throw new IllegalArgumentException("openId或buyerId不能为空");
        }
        if (Objects.isNull(getPayMoney())) {
            throw new IllegalArgumentException("支付金额不能为空");
        }
        if (getPayMoney().compareTo(BigDecimal.ZERO) <= 0) {
            throw new IllegalArgumentException("支付金额不能为0");
        }

        if (Objects.isNull(getOrderMoney())) {
            throw new IllegalArgumentException("订单金额不能为空");
        }
        if (getOrderMoney().compareTo(BigDecimal.ZERO) <= 0) {
            throw new IllegalArgumentException("订单金额不能为0");
        }
        if (StringUtils.isEmpty(getMerchantId())) {
            throw new IllegalArgumentException("商户Id不能为空");
        }
        if (StringUtils.isEmpty(getStoreCollectionId())) {
            throw new IllegalArgumentException("门店Id不能为空");
        }

    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public BigDecimal getOrderMoney() {
        return orderMoney;
    }

    public void setOrderMoney(BigDecimal orderMoney) {
        this.orderMoney = orderMoney;
    }

    public BigDecimal getPayMoney() {
        return payMoney;
    }

    public void setPayMoney(BigDecimal payMoney) {
        this.payMoney = payMoney;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getStoreCollectionId() {
        return storeCollectionId;
    }

    public void setStoreCollectionId(String storeCollectionId) {
        this.storeCollectionId = storeCollectionId;
    }
}
