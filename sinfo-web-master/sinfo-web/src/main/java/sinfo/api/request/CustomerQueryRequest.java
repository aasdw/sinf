package sinfo.api.request;

import sinfo.common.model.page.PageRequestModel;

public class CustomerQueryRequest extends PageRequestModel {
    private String keyword;

    @Override
    public void validate() throws IllegalArgumentException {

    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

}
