package sinfo.api.request;

import sinfo.common.model.page.PageRequestModel;

public class RoleQueryRequest extends PageRequestModel {
    private String keyword;

    private Integer status;

    @Override
    public void validate() throws IllegalArgumentException {

    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
