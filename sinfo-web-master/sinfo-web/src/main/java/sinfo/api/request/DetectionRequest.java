package sinfo.api.request;

import org.apache.commons.lang3.StringUtils;
import sinfo.common.request.IHttpRequest;

import java.util.Date;

public class DetectionRequest  implements IHttpRequest {



    private  Integer  id;
    private  String   score;
    private  Integer  type;
    private  String   level;
    private  String   content;
    private  String   creator;
    private  Integer  delete_flag;
    private  Date     create_date;
    private  Date     update_date;


    @Override
    public void validate() throws IllegalArgumentException {


        if (StringUtils.isEmpty(getScore())) {

            throw new IllegalArgumentException("分数不能为空");

        }


        if (StringUtils.isEmpty(String.valueOf(getType()))) {

            throw new IllegalArgumentException("类型不能为空");

        }



        if (StringUtils.isEmpty(String.valueOf(getLevel()))) {

            throw new IllegalArgumentException("级别不能为空");

        }


        if (StringUtils.isEmpty(String.valueOf(getContent()))) {

            throw new IllegalArgumentException("结果概念不能为空");

        }




    }




    public void setId(Integer id){

        this.id=id;

    }

    public Integer  getId(){

        return id;

    }



    public void setScore(String score){

        this.score=score;

    }

    public String  getScore(){


        return score;

    }


    public void setType(Integer type){


        this.type=type;

    }

    public Integer  getType(){


        return type;

    }



    public void setLevel(String  level){


        this.level=level;

    }

    public String   getLevel(){


        return level;

    }




    public void  setContent(String  content){


        this.content=content;

    }

    public String   getContent(){


        return content;

    }



    public void  setCreator(String  creator){


        this.creator=creator;

    }

    public String   getCreator(){


        return creator;

    }


    public void  setDelete_flag(Integer  delete_flag){


        this.delete_flag=delete_flag;

    }

    public Integer   getDelete_flag(){


        return delete_flag;

    }




    public void  setCreate_date(Date  create_date){


        this.create_date=create_date;

    }


    public Date   getCreate_date(){


        return create_date;

    }



    public void  setUpdate_date(Date  update_date){


        this.update_date=update_date;

    }


    public Date getUpdate_date(){


        return update_date;

    }





}
