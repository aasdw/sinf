package sinfo.api.request;

import org.apache.commons.lang3.StringUtils;
import sinfo.common.request.IHttpRequest;

import java.util.Date;

public  class RolesRequest   implements IHttpRequest {

    private Integer  id;
    private String   role_name;
    private String   role_remark;
    private Integer  role_status;
    private String   role_permissions;
    private Integer  delete_flag;
    private Date     create_date;
    private Date     update_date;



    @Override
    public void validate() throws IllegalArgumentException {


        if (StringUtils.isEmpty(getRole_name())) {

            throw new IllegalArgumentException("角色名称不能为空");

        }

        if (StringUtils.isEmpty(getRole_remark())) {

            throw new IllegalArgumentException("角色描述不能为空");

        }



        if (StringUtils.isEmpty(String.valueOf(getRole_permissions()))) {

            throw new IllegalArgumentException("角色权限不能为空");

        }



    }






    public void setId(Integer id){
        this.id=id;
    }

    public Integer getId(){

        return id;
    }


    public void setRole_name(String role_name){
        this.role_name=role_name;
    }

    public String getRole_name(){

        return role_name;
    }



    public void setRole_remark(String role_remark){
        this.role_remark=role_remark;
    }

    public String getRole_remark(){

        return role_remark;
    }


    public void setRole_status(Integer role_status){
        this.role_status=role_status;
    }

    public Integer getRole_status(){

        return role_status;
    }


    public void setRole_permissions(String role_permissions){
        this.role_permissions=role_permissions;
    }



    public String getRole_permissions(){

        return role_permissions;
    }




    public void setDelete_flag(Integer delete_flag){
        this.delete_flag=delete_flag;
    }

    public Integer getDelete_flag(){

        return delete_flag;
    }


    public void setCreate_date(Date create_date){
        this.create_date=create_date;
    }

    public Date getCreate_date(){

        return create_date;
    }



    public void setUpdate_date(Date update_date){
        this.update_date=update_date;
    }

    public Date getUpdate_date(){

        return update_date;
    }







}
