package sinfo.api.request;

import org.apache.commons.lang3.StringUtils;
import sinfo.common.request.IHttpRequest;

public class RoleRequest implements IHttpRequest {
    private String name;
    private String remark;
    private String permissions;

    @Override
    public void validate() throws IllegalArgumentException {
        if (StringUtils.isEmpty(getName())) {
            throw new IllegalArgumentException("角色名称不能为空");
        }
        if (StringUtils.isEmpty(getRemark())) {
            throw new IllegalArgumentException("角色描述不能为空");
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPermissions() {
        return permissions;
    }

    public void setPermissions(String permissions) {
        this.permissions = permissions;
    }
}
