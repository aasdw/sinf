package sinfo.api.request;

import org.apache.commons.lang3.StringUtils;
import sinfo.common.enums.LoginTypeEnums;
import sinfo.common.request.IHttpRequest;
import sinfo.common.util.ValidateUtil;

public class LoginRequest implements IHttpRequest {
    private String username;
    private String password;
    private int type;

    @Override
    public void validate() throws IllegalArgumentException {
        if (StringUtils.isEmpty(username)) {
            throw new IllegalArgumentException("手机号不能为空");
        }
        if (!ValidateUtil.isPhone(username)) {
            throw new IllegalArgumentException("手机号格式不正确");
        }

        LoginTypeEnums loginType = LoginTypeEnums.valueOf(getType());
        switch (loginType) {
            case Password:
                validateByPassword();
                break;
            case Captcha:
                validateByCaptcha();
                break;
            case Unknown:
                throw new IllegalArgumentException("手机号或密码错误");
        }
    }

    private void validateByPassword() {
        if (StringUtils.isEmpty(password)) {
            throw new IllegalArgumentException("密码不能为空");
        }
        if (!ValidateUtil.isMd5Hash(password)) {
            throw new IllegalArgumentException("手机号或密码错误");
        }
    }

    private void validateByCaptcha() {
        if (StringUtils.isEmpty(password)) {
            throw new IllegalArgumentException("短信验证码不能为空");
        }
        if (!StringUtils.isNumeric(password)) {
            throw new IllegalArgumentException("短信验证码不正确");
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
