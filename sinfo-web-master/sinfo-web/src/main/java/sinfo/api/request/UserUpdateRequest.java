package sinfo.api.request;

import org.apache.commons.lang3.StringUtils;
import sinfo.common.request.IHttpRequest;
import sinfo.common.util.ValidateUtil;

import java.util.Objects;

public class UserUpdateRequest implements IHttpRequest {
    private String userId;
    private String name;
    private String phone;
    private Integer roleId;

    @Override
    public void validate() throws IllegalArgumentException {
        if (StringUtils.isEmpty(getUserId())) {
            throw new IllegalArgumentException("员工Id不能为空");
        }
        if (StringUtils.isEmpty(getName())) {
            throw new IllegalArgumentException("员工姓名不能为空");
        }
        if (StringUtils.isEmpty(getPhone())) {
            throw new IllegalArgumentException("联系方式不能为空");
        } else {
            if (!ValidateUtil.isPhone(getPhone())) {
                throw new IllegalArgumentException("手机号格式不正确");
            }
        }
        if (Objects.isNull(getRoleId())) {
            throw new IllegalArgumentException("所属角色不能为空");
        }
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }
}
