package sinfo.api.request;

import org.apache.commons.lang3.StringUtils;
import sinfo.common.request.IHttpRequest;

import java.util.Objects;

public class UserRequest implements IHttpRequest {
    private String avator;
    private String nickName;
    private Integer sex;
    private String phone;
    private String memberId;

    @Override
    public void validate() throws IllegalArgumentException {
        if (StringUtils.isEmpty(getAvator())) {
            throw new IllegalArgumentException("avator不能为空");
        }
        if (StringUtils.isEmpty(getNickName())) {
            throw new IllegalArgumentException("nickName不能为空");
        }
        if (StringUtils.isEmpty(getPhone())) {
            throw new IllegalArgumentException("phone不能为空");
        }
        if (Objects.isNull(getSex())) {
            throw new IllegalArgumentException("sex不能为空");
        }
    }

    public String getAvator() {
        return avator;
    }

    public void setAvator(String avator) {
        this.avator = avator;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }
}
