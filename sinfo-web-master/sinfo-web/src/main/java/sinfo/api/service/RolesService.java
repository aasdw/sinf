package sinfo.api.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import jodd.util.StringUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sinfo.common.Classinstance.Detectionre;
import sinfo.common.Classinstance.Roles;

import sinfo.common.dao.RolesDao;
import sinfo.common.model.page.Pager;
import sinfo.model.entity.DetectionreEntity;
import sinfo.model.entity.RolesEntity;
import javax.annotation.Resource;

import java.util.List;


import sinfo.util.util.DateUtil;


@Service
public class RolesService   extends  ServiceImpl<RolesDao, RolesEntity> {


    @Resource
    private RolesDao rolesDao;


    DateUtil  datautil=new DateUtil();

    @Transactional(rollbackFor = Exception.class)
    public void addRole(Roles roles) throws Exception {
        RolesEntity rolesEntity = new RolesEntity();
        if (rolesDao.addroles(roles) < 1) {
            throw new Exception("添加角色失败");
        }


    }


    @Transactional(rollbackFor = Exception.class)
    public void updateRole(Roles roles) throws Exception {
        RolesEntity rolesEntity = new RolesEntity();
        //roles
        if (rolesDao.updateroles(roles) < 1) {
            throw new Exception("更新角色失败");
        }

    }


    @Transactional(rollbackFor = Exception.class)
    public void delRole(Roles roles) throws Exception {
        DetectionreEntity detectionEntity = new DetectionreEntity();

        if (rolesDao.delroles(roles) < 1) {
            throw new Exception("删除检查结果失败");
        }

    }






    public List<RolesEntity> getRolesInfo(String rolesname) throws Exception {
        List<RolesEntity> list = list();
        if (StringUtil.isNotEmpty(rolesname)) {
            list(new LambdaQueryWrapper<RolesEntity>().eq(RolesEntity::getRole_name, rolesname));
        }

        return list;


    }




    public Pager<RolesEntity, Object> getList(Integer page, Integer pageSize) throws Exception {
        List<RolesEntity> list = rolesDao.selectRoleList((page - 1) * pageSize, pageSize);
        Pager<RolesEntity, Object> pager = new Pager<>();
        pager.setList(list);
        int total = rolesDao.selectRoleListCount();
        pager.setTotal(total);
        pager.setPage(page);
        pager.setPageSize(pageSize);
        int pagerTotal = total / pageSize;
        if (total % pageSize > 0) {
            pagerTotal++;
        }
        pager.setPageTotal(pagerTotal);
        return pager;
    }





}
