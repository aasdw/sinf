package sinfo.api.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import sinfo.common.dao.WechatDeviceLoginTimeDao;
import sinfo.model.entity.WechatDeviceLoginTimeEntity;

import java.util.Objects;

@Service("wechatDeviceLoginTimeService")
public class WechatDeviceLoginTimeService extends ServiceImpl<WechatDeviceLoginTimeDao, WechatDeviceLoginTimeEntity> {

    public WechatDeviceLoginTimeEntity queryByWxIdAndDeviceId(String wechatInfoId, String deviceId) {
        return getOne(Wrappers.<WechatDeviceLoginTimeEntity>lambdaQuery().eq(WechatDeviceLoginTimeEntity::getWechatInfoId, wechatInfoId)
                .eq(WechatDeviceLoginTimeEntity::getDeviceId, deviceId));
    }

    public void clearLoginTime(String wechatInfoId, String deviceId) {
        update(Wrappers.<WechatDeviceLoginTimeEntity>lambdaUpdate().set(WechatDeviceLoginTimeEntity::getLoginTime, 0)
                .eq(WechatDeviceLoginTimeEntity::getWechatInfoId, wechatInfoId).eq(WechatDeviceLoginTimeEntity::getDeviceId, deviceId));
    }

    public void updateByWxIdAndDeviceId(String wechatInfoId, String deviceId, Integer hour) {
        WechatDeviceLoginTimeEntity wechatDeviceLoginTimeEntity = queryByWxIdAndDeviceId(wechatInfoId, deviceId);
        if (Objects.isNull(wechatDeviceLoginTimeEntity)) {
            wechatDeviceLoginTimeEntity = new WechatDeviceLoginTimeEntity();
            wechatDeviceLoginTimeEntity.setWechatInfoId(wechatInfoId);
            wechatDeviceLoginTimeEntity.setDeviceId(deviceId);
            wechatDeviceLoginTimeEntity.setLoginTime(hour);
            save(wechatDeviceLoginTimeEntity);
        } else {
            wechatDeviceLoginTimeEntity.setLoginTime(wechatDeviceLoginTimeEntity.getLoginTime() + hour);
            updateById(wechatDeviceLoginTimeEntity);
        }
    }
}
