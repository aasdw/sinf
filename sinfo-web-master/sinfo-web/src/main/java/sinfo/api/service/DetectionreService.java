package sinfo.api.service;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import jodd.util.StringUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sinfo.api.request.DetectionRequest;
import sinfo.api.request.UserRequest;
import sinfo.common.Classinstance.Detectionre;
import sinfo.common.dao.DetectionreDao;


import sinfo.common.model.page.Pager;
import sinfo.model.entity.DetectionreEntity;
import sinfo.model.entity.UserEntity;
import sinfo.util.util.RandomUtil;


import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static org.springframework.beans.BeanUtils.copyProperties;


@Service
public class DetectionreService    extends ServiceImpl<DetectionreDao, DetectionreEntity> {


    @Resource
    private DetectionreDao detectionreDao;


    @Transactional(rollbackFor = Exception.class)
    public void addDetection(Detectionre detec) throws Exception {
        DetectionreEntity detectionEntity = new DetectionreEntity();

        if (detectionreDao.adddetection(detec) < 1) {
            throw new Exception("添加检查结果失败");
        }

    }




    @Transactional(rollbackFor = Exception.class)
    public void delDetection(Detectionre detec) throws Exception {
        DetectionreEntity detectionEntity = new DetectionreEntity();

        if (detectionreDao.deldetection(detec) < 1) {
            throw new Exception("删除检查结果失败");
        }

    }




    @Transactional(rollbackFor = Exception.class)
    public void upDetection(Detectionre detec) throws Exception {
        DetectionreEntity detectionEntity = new DetectionreEntity();

        if (detectionreDao.updetection(detec) < 1) {
            throw new Exception("更新检查结果失败");
        }

    }





    public List<Detectionre> getDetection(Detectionre detec) throws Exception {
        DetectionreEntity detectionEntity = new DetectionreEntity();

        if (detectionreDao.selectDetectionreList(detec).size()<=0) {
            throw new Exception("获取检查结果失败");
        }

        return detectionreDao.selectDetectionreList(detec);

    }



    public  List<DetectionreEntity> getDetectionInfo(String   creator) throws Exception {
       List<DetectionreEntity> list = list();
        if (StringUtil.isNotEmpty(creator)) {
            list(new LambdaQueryWrapper<DetectionreEntity>().eq(DetectionreEntity::getCreator,creator));
        }

        return list;

}




public Pager<DetectionreEntity, Object> getList(Integer page, Integer pageSize) throws Exception {
        List<DetectionreEntity> list = detectionreDao.selectDetectionreList((page - 1) * pageSize, pageSize);
        Pager<DetectionreEntity, Object> pager = new Pager<>();
        pager.setList(list);
        int total = detectionreDao.selectDetectionreListCount();
        pager.setTotal(total);
        pager.setPage(page);
        pager.setPageSize(pageSize);
        int pagerTotal = total / pageSize;
        if (total % pageSize > 0) {
            pagerTotal++;
        }
        pager.setPageTotal(pagerTotal);
        return pager;
}











}
