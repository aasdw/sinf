package sinfo.api.service;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import jodd.util.StringUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sinfo.api.request.UserRequest;
import sinfo.common.dao.UserDao;
import sinfo.common.model.page.Pager;
import sinfo.model.entity.UserEntity;
import sinfo.util.util.RandomUtil;

import javax.annotation.Resource;
import java.util.List;

import static org.springframework.beans.BeanUtils.copyProperties;

@Service
public class UserService extends ServiceImpl<UserDao, UserEntity> {

    @Resource
    private UserDao userDao;

    @Transactional(rollbackFor = Exception.class)
    public void addUser(UserRequest request) throws Exception {
        UserEntity userEntity = new UserEntity();
        userEntity.setMemberId(RandomUtil.getUUID());
        copyProperties(request, userEntity);
        if (!save(userEntity)) {
            throw new Exception("添加用户信息失败");
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void updateUser(UserRequest request) throws Exception {
        UserEntity userEntity = new UserEntity();
        copyProperties(request, userEntity);
        if (!updateById(userEntity)) {
            throw new Exception("修改用户信息失败");
        }
    }

    public UserEntity queryUserInfoById(String memberId) throws Exception {
        return getById(memberId);
    }

    public List<UserEntity> getUserInfo(String memberId) throws Exception {
        List<UserEntity> list = list();
        if (StringUtil.isNotEmpty(memberId)) {
            list(new LambdaQueryWrapper<UserEntity>().eq(UserEntity::getMemberId, memberId));
        }
        return list;
    }

    public Pager<UserEntity, Object> getList(Integer page, Integer pageSize) throws Exception {
        List<UserEntity> list = userDao.selectMemberList((page - 1) * pageSize, pageSize);
        Pager<UserEntity, Object> pager = new Pager<>();
        pager.setList(list);
        int total = userDao.selectMemberListCount();
        pager.setTotal(total);
        pager.setPage(page);
        pager.setPageSize(pageSize);
        int pagerTotal = total / pageSize;
        if (total % pageSize > 0) {
            pagerTotal++;
        }
        pager.setPageTotal(pagerTotal);
        return pager;
    }

}
