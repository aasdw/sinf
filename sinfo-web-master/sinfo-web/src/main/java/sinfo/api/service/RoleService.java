package sinfo.api.service;


import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import sinfo.api.request.RoleQueryRequest;
import sinfo.api.request.RoleRequest;
import sinfo.common.bo.RoleBo;
import sinfo.common.dao.RoleDao;
import sinfo.common.model.page.Pager;
import sinfo.common.model.page.PagerBuilder;
import sinfo.common.util.StringUtil;
import sinfo.model.entity.RoleEntity;
import sinfo.model.vo.RoleVo;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.beans.BeanUtils.copyProperties;

@Service
public class RoleService extends ServiceImpl<RoleDao, RoleEntity> {

    @Resource
    private RoleDao roleDao;

    public void addOrUpdate(RoleRequest request) throws Exception {
        RoleBo roleBo = new RoleBo();
        copyProperties(request, roleBo);
        if (roleDao.addOrUpdate(roleBo) < 1) {
            throw new Exception("添加或修改角色失败");
        }
    }

    public RoleVo queryRoleInfo(Integer id) throws Exception {
        RoleEntity sysUserEntity = getOne(Wrappers.<RoleEntity>lambdaQuery().eq(RoleEntity::getId, id)
                .eq(RoleEntity::getDeleteFlag, 0));
        if (StringUtil.isNull(sysUserEntity)) {
            throw new Exception("该角色不存在");
        }
        RoleVo roleVo = new RoleVo();
        copyProperties(sysUserEntity, roleVo);
        int number = roleDao.selectSysUserNumber(id);
        roleVo.setNumber(number);
        return roleVo;
    }

    public void delete(Integer id) throws Exception {
        //String userId = SessionUtil.getUserId();
        RoleEntity sysUserEntity = getOne(Wrappers.<RoleEntity>lambdaQuery().eq(RoleEntity::getId, id)
                .eq(RoleEntity::getDeleteFlag, 0));
        if (StringUtil.isNull(sysUserEntity)) {
            throw new Exception("该角色不存在");
        }
        boolean flag = update(Wrappers.<RoleEntity>lambdaUpdate()
                .set(RoleEntity::getDeleteFlag, 1)
                .eq(RoleEntity::getId, id));
        if (!flag) {
            throw new Exception("删除角色失败");
        }
    }

    public Pager<RoleVo, Object> getList(RoleQueryRequest request) throws Exception {
        PagerBuilder<RoleVo, Object> pagerBuilder = new PagerBuilder<>(request.getPage(), request.getPageSize());
        List<RoleVo> beanList = new ArrayList<>();
        pagerBuilder.setListPagerCallback((offset, amount) -> {
            List<RoleEntity> list = roleDao.selectRoleList(request.getKeyword(), request.getStatus(), offset, amount);
            for (RoleEntity model : list) {
                RoleVo roleVo = new RoleVo();
                copyProperties(model, roleVo);
                int number = roleDao.selectSysUserNumber(model.getId());
                roleVo.setNumber(number);
                beanList.add(roleVo);
            }
            return beanList;
        });
        pagerBuilder.setRecordTotalCallback(() -> roleDao.selectRoleListCount(request.getKeyword(), request.getStatus()));
        return pagerBuilder.build();
    }

}
