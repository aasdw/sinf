package sinfo.api.service;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import sinfo.api.request.CustomerQueryRequest;
import sinfo.common.dao.WxUsesrInfoDao;
import sinfo.common.model.page.Pager;
import sinfo.common.model.page.PagerBuilder;
import sinfo.model.entity.WxUserInfoEntity;
import sinfo.model.enums.WxUserSexType;
import sinfo.model.vo.CustomerVo;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.beans.BeanUtils.copyProperties;

@Service
public class CustomerService extends ServiceImpl<WxUsesrInfoDao, WxUserInfoEntity> {

    @Resource
    private WxUsesrInfoDao wxUsesrInfoDao;

    public Pager<CustomerVo, Object> getList(CustomerQueryRequest request) throws Exception {
        PagerBuilder<CustomerVo, Object> pagerBuilder = new PagerBuilder<>(request.getPage(), request.getPageSize());
        List<CustomerVo> beanList = new ArrayList<>();
        pagerBuilder.setListPagerCallback((offset, amount) -> {
            List<WxUserInfoEntity> list = wxUsesrInfoDao.selectCustomerList(request.getKeyword(), offset, amount);
            for (WxUserInfoEntity model : list) {
                CustomerVo customerVo = new CustomerVo();
                copyProperties(model, customerVo);
                WxUserSexType wxUserSexType = WxUserSexType.valueOf(model.getSex());
                customerVo.setSex(wxUserSexType.getName());
                beanList.add(customerVo);
            }
            return beanList;
        });
        pagerBuilder.setRecordTotalCallback(() -> wxUsesrInfoDao.selectCustomerListCount(request.getKeyword()));
        return pagerBuilder.build();
    }

}
