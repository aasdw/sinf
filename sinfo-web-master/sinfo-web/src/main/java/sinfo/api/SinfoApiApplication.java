package sinfo.api;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
        (exclude = {DataSourceAutoConfiguration.class, MongoAutoConfiguration.class}, scanBasePackages = {"link.sinfo"})
@ComponentScan(basePackages = {"sinfo"})
@MapperScan({"sinfo.common.dao", "sinfo.api.dao"})
public class SinfoApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(SinfoApiApplication.class, args);
    }
}
