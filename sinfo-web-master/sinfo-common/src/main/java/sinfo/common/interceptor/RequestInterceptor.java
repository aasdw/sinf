package sinfo.common.interceptor;

import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import sinfo.common.annotation.Authentication;
import sinfo.common.annotation.ServerAuthentication;
import sinfo.common.annotation.WithoutAuthentication;
import sinfo.common.enums.AuthenticationType;
import sinfo.common.exceptions.NotLoginException;
import sinfo.common.constant.Constants;
import sinfo.common.util.JwtClientTokenUtil;
import sinfo.common.util.JwtServerTokenUtil;
import sinfo.common.util.SessionUtil;
import sinfo.util.util.CacheManagerUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Objects;

@Component
public class RequestInterceptor implements HandlerInterceptor {
    private final static Logger logger = LoggerFactory.getLogger(RequestInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IllegalArgumentException, NotLoginException {
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();
        WithoutAuthentication annotation = method.getAnnotation(WithoutAuthentication.class);
        if (Objects.isNull(annotation)) {
            //校验token
            if (method.getAnnotation(Authentication.class) != null || method.getDeclaringClass().getAnnotation(Authentication.class) != null) {
                if (!checkToken(request, AuthenticationType.Client.getType())) {
                    throw new NotLoginException();
                }
            } else if (method.getAnnotation(ServerAuthentication.class) != null || method.getDeclaringClass().getAnnotation(ServerAuthentication.class) != null) {
                if (!checkToken(request, AuthenticationType.Server.getType())) {
                    throw new NotLoginException();
                }
            }
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }

    private boolean checkToken(HttpServletRequest request, String type) {
        try {

            String token = request.getHeader(Constants.HEADER_OPEN_API_TOKEN);
            if (StringUtils.isEmpty(token)) {
                throw new IllegalArgumentException(Constants.HEADER_OPEN_API_TOKEN + "不能为空");
            }
            String version = request.getHeader(Constants.HEADER_OPEN_API_VERSION);
            if (StringUtils.isEmpty(version)) {
                throw new IllegalArgumentException(Constants.HEADER_OPEN_API_VERSION + "不能为空");
            }
            if (!version.equals(Constants.WXMT_VERSION)) {
                throw new IllegalArgumentException("版本号不正确");
            }
            if (type.equals(AuthenticationType.Client.getType())) {
                DecodedJWT jwtToken = JwtClientTokenUtil.getClientToken(token);
                Claim memberIdClaim = jwtToken.getClaim("memberId");
                Claim appIdClaim = jwtToken.getClaim("appId");

                if (Objects.isNull(memberIdClaim) || Objects.isNull(appIdClaim)) {
                    return false;
                }
                String memberId = memberIdClaim.asString();
                String appId = appIdClaim.asString();
                String tokenKey = JwtClientTokenUtil.getTokenKey(appId, memberId);
                String tokenCache = CacheManagerUtil.getCacheManager().get(tokenKey);
                if (StringUtils.isEmpty(tokenCache) || !token.equals(tokenCache)) {
                    return false;
                }
                SessionUtil.setCurrentCustomerId(appId);
                SessionUtil.setCurrentMemberId(memberId);
                CacheManagerUtil.getCacheManager().expire(tokenKey, Constants.REDIS_KEY_CLIENT_TOKEN_EXPIRE);
            } else {
                DecodedJWT jwtToken = JwtServerTokenUtil.getServerToken(token);
                Claim appIdClaim = jwtToken.getClaim("appId");
                if (Objects.isNull(appIdClaim)) {
                    return false;
                }
                String appId = appIdClaim.asString();
                String tokenKey = JwtServerTokenUtil.getTokenKey(appId);
                String tokenCache = CacheManagerUtil.getCacheManager().get(tokenKey);
                if (StringUtils.isEmpty(tokenCache) || !token.equals(tokenCache)) {
                    return false;
                }
                SessionUtil.setCurrentCustomerId(appId);
                CacheManagerUtil.getCacheManager().expire(tokenKey, Constants.REDIS_KEY_SERVER_TOKEN_EXPIRE);
            }
        } catch (Exception e) {
            logger.error("校验权限错误", e);
            return false;
        }
        return true;
    }
}
