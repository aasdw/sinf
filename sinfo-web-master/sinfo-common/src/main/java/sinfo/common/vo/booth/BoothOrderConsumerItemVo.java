package sinfo.common.vo.booth;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class BoothOrderConsumerItemVo {
    private String storeName;
    private BigDecimal payMoney;
    private BigDecimal originalMoney;
    private BigDecimal discount;
    private String industryName;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime payTime;
    private String payStatus;
    private BigDecimal score;
}
