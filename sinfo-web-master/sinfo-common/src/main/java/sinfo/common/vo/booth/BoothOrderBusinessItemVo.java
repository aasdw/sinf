package sinfo.common.vo.booth;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class BoothOrderBusinessItemVo {
    private String orderId;
    private String qRCodeName;
    private String collectionMoney;
    private String originalMoney;
    private String discount;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime payTime;
    private String payStatus;
    private String score;
}
