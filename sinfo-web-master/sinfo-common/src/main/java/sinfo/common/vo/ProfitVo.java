package sinfo.common.vo;

import lombok.Data;

import java.math.BigDecimal;
@Data
public class ProfitVo {
    private String day;
    private String money;
}
