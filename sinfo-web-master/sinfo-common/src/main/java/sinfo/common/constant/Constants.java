package sinfo.common.constant;

public class Constants {
    public static final String REDIS_PREFIX = "qhCloud:wxmt:";

    public static final String LAST_API_REQUEST_TIME = ":api:request:time";

    public static final String API_REQUEST_AUTHORITY = ":api:request:authority";

    public static final String User_AUTHORITY = ":user:authority";

    public static final String SERVER_AUTH_TOKEN = "server:authToken:";
    public static final String CLIENT_AUTH_TOKEN = "client:authToken:";

    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String TOKEN_HEADER = "token";

    public static final String HEADER_OPEN_APP_ID = "X-App-Id";
    public static final String HEADER_OPEN_APP_SECRET = "X-App-Secret";
    public static final String HEADER_OPEN_ACCESS_SECRET = "X-Access-Secret";
    public static final String HEADER_OPEN_API_VERSION = "X-Api-Version";
    public static final String HEADER_OPEN_API_TOKEN = "X-Api-Token";
    public static final String JWT_TOKEN_ISSUER = "QingHuo Wxmt";
    public static final String JWT_CLIENT_TOKEN_SECRET = "EpgMBeDZxZNBTH8EwkUVyWzf2qCatkvY";
    public static final String JWT_SERVER_TOKEN_SECRET = "QS8Uu51dp2U2Oy9T89LtYr76ex9325Mj";
    public static final String WXMT_VERSION = "1.0";

    public static final int REDIS_KEY_SERVER_TOKEN_EXPIRE = 30 * 24 * 3600;
    public static final int REDIS_KEY_CLIENT_TOKEN_EXPIRE = 24 * 3600;

    public static final String MESSAGE_CALLBACK_DISTINCT = "message:callback:distinct";

    public static final long SECOND_MILLISECONDS = 1000L;
    public static final long MINUTE_MILLISECONDS = 60 * 1000L;
    public static final long HOUR_MILLISECONDS = 60 * 60 * 1000L;
    public static final long DAY_MILLISECONDS = 24 * 60 * 60 * 1000L;
    public static final int MINUTE_SECONDS = 60;
    public static final int HOUR_SECONDS = 60 * 60;
    public static final int MAX_SEND_TASK_COUNT = 200;
    public static final int VALID_TIME = 86400000;

    public static final String WECHAT_OFFLINE_MESSAGE = "已离线";

    /**
     * 自动回复最大消息条数
     */
    public static final int WECHAT_MAX_AUTO_REPLY_COUNT = 1;

    /**
     * 关键词最大条数
     */
    public static final int WECHAT_MAX_KEYWORD_COUNT = 3;

    public static final String CHATROOM_WELCOME_BE_INVITED = "@被邀请者";
    public static final String CHATROOM_KICK_TEMPLATE = "@被踢者";

    public static final String CHATROOM_ANALYSIS_SHARE_URL = ":chatroom:analysis:share:url";

    public static final String CHATROOM_ANALYSIS_REQUEST_CACHE = "chatroom:analysis:cache";

    public static final String SECRET = "secret";

    /**
     * 最大添加群组数量
     */
    public static final int MAX_GROUP_COUNT = 50;

    public static final String CONFIRM_ADD_FRIEND_MESSAGE_FIRST = "你已添加了";
    public static final String CONFIRM_ADD_FRIEND_MESSAGE_SECOND = "开始聊天";
    public static final String CONFIRM_ADD_FRIEND_MESSAGE_THIRD = "朋友验证请求";

    public static final String QUERY_LABEL_CONTACT_LIST = "query:label:contact:list";
    /**
     * 查询群或者好友信息
     */
    public static final String QUERY_CONTACT_LIST = "query:contact:list";
    public static final String REQUEST_CUSTOMER_ID = "customerId";
    public static final String REQUEST_MEMBER_ID = "memberId";

    public static final String ACTIVITY_TITLE = "活动";

    /**
     * 查询指定时间内的踢人记录（小时）
     */
    public static final int CHATROOM_KICK_QUERY_HOURS = 48;

    public static final String XML_LABEL_START = "<?xml";
    public static final String MSG_LABEL_START = "<msg>";
    public static final String MSG_LABEL_END = "</msg>";
    public static final String SYS_MSG_LABEL_START = "<sysmsg";
    public static final String SYS_MSG_LABEL_END = "</sysmsg>";
    public static final String APP_MSG_LABEL_START = "<appmsg";
    public static final String TYPE_LABEL_START = "<type>";
    public static final String TYPE_LABEL_END = "</type>";
    public static final String CDATA_START = "<![CDATA[";
    public static final String CDATA_END = "]]>";
    public static final String MD5_PATTERN = "md5=\"\\w+\"";
    public static final String LEN_PATTERN = "len=\"\\w+\"";
    public static final String LENGTH_PATTERN = "\\slength=\"\\w+\"";
    public static final String VOICE_LENGTH_PATTERN = "voicelength=\"\\w+\"";
    public static final String VOICE_BUFID_PATTERN = "bufid=\"\\w+\"";

    public static final String CUSTOMER_RECHARGE = "客户充值";

    public static final String WECHAT_ORIGINAL_ID = "wxid_";

    public static final String CHATROOM_UN_NAMED = "未命名";

    public static final String CHATROOM_DELETE = "移出群聊";

    /***
     * 活动支付类型
     * 1零钱支付，2货款支付，3微信公众号支付，4微信小程序支付，5微信App支付，6微信H5支付，7支付宝App支付，8支付宝H5支付，9易宝微信小程序支付，10易宝微信公众号支付，11易宝微信App支付，12易宝银联支付，13线下支付
     */
    public static final String[] activityPayType = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"};

    public static final String CHATROOM_ID_SUFFIX = "@chatroom";

    public static final String CHATROOM_MODIFY_NAME = "你修改群名为";

    public static final String CHATROOM_MODIFY_NAME_XML = "修改群名为";

    public static final String GONGMALL_AUTONYM_URL = "https://contract-qa.gongmall.com/url_contract.html?companyId=PNKmgM&positionId=nP18WV&data=";

    public static final String GONGMALL_APPKEY = "62fa691100384854a5087cc3839afa0c";

    public static final String GONGMALL_APPSECRET = "86adc7ca517c5408dd151a3c79e07155";

    public static final String PREFIX = "ZDB";

    public static final String ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s";

    public static final String USER_INFO_URL = "https://api.weixin.qq.com/cgi-bin/user/info?&access_token=%s&openid=%s&lang=zh_CN";

    public static final String WX_H5_ACCREDIT_URL = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s&response_type=code&scope=%s&state=jetcms#wechat_redirect";

    public static final String USER_ACCREDIT_INFO_URL = "https://api.weixin.qq.com/sns/userinfo?lang=zh_CN&access_token=%s&openid=%s";

    public static final String GET_OPENID_URL = "https://api.weixin.qq.com/sns/oauth2/access_token?grant_type=authorization_code&appid=%s&secret=%s&code=%s";

    public static final String HJ_PAY_KEY = "e3af5dc4002a403f8347b147f69c65b7";

    public static final String SUCCESS = "success";

    public static final String FAIL = "fail";

    public static final int PAY_STATUS = 100;

    public static final  Integer DELETE = 1;//删除

    public static final  Integer UN_DELETE = 0;//未删除

    public static final String PRE_KEY = "zdb:";

    public static final String JWT_TOKEN_SECRET ="" ;
}
