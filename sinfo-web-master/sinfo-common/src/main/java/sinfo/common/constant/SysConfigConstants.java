package sinfo.common.constant;

public class SysConfigConstants {
    /**
     * 机器人每日续费消耗点数
     */
    public static final String ROBOT_DAY_CONSUME_POINT = "robot.day.consume.point";
    /**
     * 微信群每日续费消耗点数
     */
    public static final String CHATROOM_DAY_CONSUME_POINT = "chatRoom.day.consume.point";
    /**
     * 课程转播购买单个转播群所需点数
     */
    public static final String LESSON_BROADCAST_BUY_POINT = "lesson.broadcast.buy.point";
    /**
     * 微信群每日最多自动加好友数量
     */
    public static final String CHATROOM_DAY_ADD_FRIEND_COUNT = "chatRoom.day.addFriend.count";
    /**
     * 微信接口访问需要挂号时间
     */
    public static final String WECHAT_LOGIN_TIME = "wechat.login.time";
    /**
     * 微信发送朋友圈需要挂号时间
     */
    public static final String WECHAT_FRIEND_CIRCLE_LOGIN_TIME = "wechat.friendCircle.login.time";
    /**
     * 青火企业微信appid
     */
    public static final String QINGHUO_APPID_KEY = "qinghuo.appid";
    /**
     * 青火企业微信secret
     */
    public static final String QINGHUO_SECRET_KEY = "qinghuo.secret";
    /**
     * 急速扩群活动回调地址
     */
    public static final String ACTIVITY_REDIRECT_URL_KEY = "activity.redirect_url";

    /**
     * 提现手续费
     */
    public static final String WITHDRAW_POUNDAGE_RATIO_KEY = "withdraw.poundage.ratio";

    /**
     * 支付宝商户的appid
     */
    public static final String APP_ID = "2021002150684613";//2088141231158578  2021002150684613

    /**
     * 支付宝商户的私钥
     */
    public static final String MERCHANT_PRIVATE_KEY = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCtEuNCUM+qYJJqYy7236snICSrbFreygwgIHryKWvJSif75167S8t6v80aHtTnzJCf05hkkv6pZwpRzqnjxvJGBOBc4DDD0Dj4tNTQufXuq69uGhR8Ws+CdTc7fhuOzSqkmVuloweezzaVTs3HyDRjBIc9wfOTTWAWdNAQn7IPO3U5KeLWDshoDhV7zSzIKM9xVRxwodHO2V2WeUZLflB/G+Vsg715l8p4oy2oHJkkFrV7CuxmgoAhfjc4zmn6kume1or+8t6CJMcdw0XlZDJjHz0CzA5LK1Cny1kpqbcBUnZfPtIcjMsjgnlC+EQgz8nCgkT4sh9op+0Lo31SWEVHAgMBAAECggEAfF+uYOGh2lY74agU/+QMIrm+oFHlm2SnAg9VSuDpTGW2Bhhmw0HcoAFlavftGAaxh6mPQfdAeoxJpjBVdhJp2Fd4xEV3xb/EsTWCGd0JFtLZ5s1nG1ZG+QEzB9XACAb4NgDfE1DtEPrnxrQyvPbqbp26cm8GkzxvaLPZMZkRZE+tOIQ5u0zSOoMXnTvutaKoX4k0WOiO+g1boLtmdthfapSads3l7oRd0aQeqDrfMNAk5OHNAZfdfoBkDmaPY5eIAXkAUq/ueYTlGUsn3Otlh09MTP9ml3TMe1SHy8BlEmGj/83sBqdvKYNr2Cas6Sh8N4DdTjybSZoIHqv9cg56wQKBgQDuTv0XqesHv5R3jRI3d4lduTiEMKyUIpdGJSpA7Pp4E5ue9CLMBazTTByn3i7p8zsBCcVa2D1HURtIc+CiO4Wm2YURDXhIdsc/+gfXh91K2VGmidQag0rLLMLybvZIFDQNssW6YsWXqw97xCH/zeZdJliKkV5FE3SsK0UH3w1zZQKBgQC57B+8WK1Egp0W84m2COhWRI/PgjQUCv1lmMFkgOsiHK7ob4yTt5zMWfUa4J6hbepqW+1ptFZcOF9EGDus0SXPh0I/IXEv2GHA5fssvr4/lP8C5wK3mlRCQEc4lulDmXWbPVtT4oyB3raf8DvdhlN031IZpF76FkqCRmhOwj2pOwKBgQCPkeDir8jcCyxr9f6Cki6c22jUIurl+A0BAoT5RowDaXS5B8Yq+kRLrDAbZj0HTzU+A9/1Qg9w9skkFkPAGXIEuklzo5TSu1SBE6ZD0COe41xQo/q32l9mFlqAah9P/3P4yLpc9wKpxDH404nULfzaI672FGwh233P9+WHv3bADQKBgEDX2oWtA9k2GKWtkKT9XFLiHKoJxjILVy885I4x7E/8YwBMD2Jhkn0vDq4uEwVIOY6Yuye3rrsBKjCspvtyAlbr3K358uj19bn6O+zyJZY1V+9iNG9mkuZ8Vpqk/3wFJKBc+OI7zDdrPpRvi+GJ/MdxL8gjrMBaT+g5uB1n32uLAoGBANp68VY0Hhi6GXEepjbO8toNwjlWiHm6YdPKdyYTyr5FJ5SBAYOY9OJ4nDp7jrNRR5Nk/WMGTFUfLwntidq2+PmwvaknGuBdTvRp4UctcniygHlsVUEn0SQxQ3Y74iJpgFx2bdM802J5w0wp3192Vwr637DxXVlCDqv3YlsInECT";

    /**
     * 支付宝商户的公钥
     */
    //public static final String ALIPAY_PUBLIC_KEY  = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArRLjQlDPqmCSamMu9t+rJyAkq2xa3soMICB68ilryUon++deu0vLer/NGh7U58yQn9OYZJL+qWcKUc6p48byRgTgXOAww9A4+LTU0Ln17quvbhoUfFrPgnU3O34bjs0qpJlbpaMHns82lU7Nx8g0YwSHPcHzk01gFnTQEJ+yDzt1OSni1g7IaA4Ve80syCjPcVUccKHRztldlnlGS35QfxvlbIO9eZfKeKMtqByZJBa1ewrsZoKAIX43OM5p+pLpntaK/vLegiTHHcNF5WQyYx89AswOSytQp8tZKam3AVJ2Xz7SHIzLI4J5QvhEIM/JwoJE+LIfaKftC6N9UlhFRwIDAQAB";

    /**
     * 支付宝商户的公钥
     */
    public static final String ALIPAY_PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnxj/9qwVfgoUh/y2W89L6BkRAFljhNhgPdyPuBV64bfQNN1PjbCzkIM6qRdKBoLPXmKKMiFYnkd6rAoprih3/PrQEB/VsW8OoM8fxn67UDYuyBTqA23MML9q1+ilIZwBC2AQ2UBVOrFXfFl75p6/B5KsiNG9zpgmLCUYuLkxpLQIDAQAB";

    /**
     * 支付宝网关
     */
    public static final String GATEWAY_URL = "https://openapi.alipay.com/gateway.do";


    /**
     * 支付宝网关
     */
    public static final String ZDB_DEV_H5_KEY = "zdb.dev.h5";
    /**
     * 支付宝网关
     */
    public static final String ZDB_DEV_APPID_KEY = "zdb.dev.appid";
    /**
     * 支付宝网关
     */
    public static final String ZDB_DEV_SECRET_KEY = "zdb.dev.secret";


    /**
     * 收款H5url
     */
    public static final String COLLECTION_H5_URL_KEY = "collection.h5.url";

    /**
     * 折多宝LOGO
     */
    public static final String ZDB_LOGO_URL_KEY = "zdb.logo.url";


    /**
     * 商户入件平台审核时间
     */
    public static final String AUDIT_DATE_KEY = "audit.date";
    /**
     * 积分计算
     */
    public static final String MEMBER_SCORE_COEFFICIENT = "member.score.coefficient";
    /**
     * 奖金提现手续费
     */
    public static final String ZDB_PROFIT_WITHDRAW_CHARGE = "zdb.profit.withdraw.charge";

    /**
     * 商户编号
     */
    public static final String WXPAY_MERCHANT_KEY = "wxpay.merchant";

    public static final String ZDB_DEVICE_APPID = "zdb.device.appid";

    public static final String ZDB_DEVICE_SECRET = "zdb.device.secret";

    public static final String ZDB_DEVICE_CONTENT = "zdb.device.content";

    public static final String ZDB_DEVICE_API = "zdb.device.api";




}
