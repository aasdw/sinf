package sinfo.common.configuration;

import sinfo.common.properties.RedisClusterProperties;
import sinfo.common.manager.RedisCacheManager;
import sinfo.util.cache.ICacheManager;
import sinfo.util.cache.redis.RedisClusterBuilder;
import sinfo.util.cache.redis.RedisConfig;
import sinfo.util.util.CacheManagerUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;
import redis.clients.jedis.JedisCluster;

import javax.annotation.Resource;

//@Configuration
public class RedisConfiguration {

    @Resource
    RedisClusterProperties redisClusterProperties;

    @Bean
    public JedisCluster getJedisCluster() {
        RedisConfig redisConfig = new RedisConfig();
        BeanUtils.copyProperties(redisClusterProperties, redisConfig);
        return new RedisClusterBuilder(redisConfig).build();
    }

    @Bean
    public ICacheManager cacheManager(JedisCluster jedisCluster) {
        RedisCacheManager redisCacheManager = new RedisCacheManager(jedisCluster);
        CacheManagerUtil.setCacheManager(redisCacheManager);
        return redisCacheManager;
    }
}
