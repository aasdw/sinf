package sinfo.common.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import sinfo.common.constant.Constants;
import sinfo.common.interceptor.RequestInterceptor;
import sinfo.util.jackson.JacksonObjectMapper;
import sinfo.util.util.SpringUtil;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

@Configuration
public class ApiConfiguration implements WebMvcConfigurer {

    @Resource
    RequestInterceptor requestInterceptor;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedHeaders("Origin", "Content-Type", "Accept", "X-Requested-With", Constants.TOKEN_HEADER)
                .allowedOriginPatterns("*")
                .allowedMethods("GET", "POST", "PUT", "PATCH", "DELETE")
                .allowCredentials(true)
                .exposedHeaders(Constants.TOKEN_HEADER);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(requestInterceptor)
                .excludePathPatterns("/static/*")
                .excludePathPatterns("/error")
                .addPathPatterns("/**")
                .order(1);
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        MappingJackson2HttpMessageConverter jackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
        jackson2HttpMessageConverter.setObjectMapper(getJacksonObjectMapper());
        jackson2HttpMessageConverter.setSupportedMediaTypes(Collections.singletonList(MediaType.APPLICATION_JSON));
        converters.add(jackson2HttpMessageConverter);
    }

    @Bean
    public ObjectMapper getJacksonObjectMapper() {
        return new JacksonObjectMapper();
    }

    @Bean
    public SpringUtil getSpringUtil() {
        return new SpringUtil();
    }

}
