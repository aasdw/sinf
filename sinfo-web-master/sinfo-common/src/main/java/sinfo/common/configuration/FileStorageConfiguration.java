package sinfo.common.configuration;

import sinfo.common.properties.AliyunProperties;
import sinfo.util.filesystem.AliOssFileStorage;
import sinfo.util.filesystem.IFileStorage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

@Configuration
public class FileStorageConfiguration {

    @Resource
    AliyunProperties aliyunProperties;

    @Bean
    public IFileStorage fileStorage() {
        return new AliOssFileStorage(aliyunProperties.getOssEndpoint(), aliyunProperties.getAccessKeyId(), aliyunProperties.getAccessKeySecret(), aliyunProperties.getOssBucket());
    }


}
