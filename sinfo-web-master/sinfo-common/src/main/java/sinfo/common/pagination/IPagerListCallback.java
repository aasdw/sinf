package sinfo.common.pagination;

public interface IPagerListCallback<T> {

    T resolve(int offset, int amount) throws Exception;
}
