package sinfo.common.pagination;

public interface IPagerCallback<T> {

    T resolve() throws Exception;
}
