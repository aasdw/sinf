package sinfo.common.manager;

import sinfo.common.constant.Constants;
import sinfo.util.cache.ICacheManager;
import sinfo.util.cache.ICacheResolver;
import sinfo.util.util.JacksonUtil;
import org.apache.commons.lang3.StringUtils;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.Tuple;
import redis.clients.jedis.params.SetParams;

import java.util.*;
import java.util.stream.Collectors;

public class RedisCacheManager implements ICacheManager {

    private final JedisCluster jedisCluster;

    public RedisCacheManager(JedisCluster jedisCluster) {
        this.jedisCluster = jedisCluster;
    }
    @Override
    public JedisCluster getJedisCluster() {
        return jedisCluster;
    }

    @Override
    public Set<String> keys(String pattern) {
        if (StringUtils.isEmpty(pattern)) {
            return new HashSet<>();
        }
        Set<String> keys = jedisCluster.keys(Constants.REDIS_PREFIX + pattern + ":*");

        return keys.stream().map(key -> key.replace(Constants.REDIS_PREFIX, "")).collect(Collectors.toSet());
    }

    @Override
    public String get(String key) {
        String value = jedisCluster.get(Constants.REDIS_PREFIX + key);
        return StringUtils.trimToEmpty(value);
    }

    @Override
    public <T> T get(String key, Class<T> classOfType) {
        return JacksonUtil.fromJson(get(key), classOfType);
    }

    @Override
    public <T> List<T> getList(String key, Class<T> classOfType) {
        return JacksonUtil.convertList(get(key), classOfType);
    }

    @Override
    public long getNumber(String key) {
        String value = get(key);
        if (StringUtils.isEmpty(value)) {
            return 0L;
        }
        try {
            return Long.parseLong(value);
        } catch (NumberFormatException ex) {
            return 0L;
        }
    }

    private String convertValueToSet(Object value) {
        return JacksonUtil.toJson(value);
    }

    @Override
    public void set(String key, Object value) {
        jedisCluster.set(Constants.REDIS_PREFIX + key, convertValueToSet(value));
    }

    @Override
    public void set(String key, Object value, int seconds) {
        set(key, value);
        if (seconds == 0) {
            return;
        }
        expire(key, seconds);
    }

    @Override
    public void setString(String key, String value) {
        jedisCluster.set(Constants.REDIS_PREFIX + key, value);
    }

    @Override
    public void setString(String key, String value, int seconds) {
        setString(key, value);
        if (seconds == 0) {
            return;
        }
        expire(key, seconds);
    }

    @Override
    public long expire(String key, int seconds) {
        return jedisCluster.expire(Constants.REDIS_PREFIX + key, seconds);
    }

    @Override
    public void del(String key) {
        jedisCluster.del(Constants.REDIS_PREFIX + key);
    }

    @Override
    public long incrBy(String key, long delta) {
        return jedisCluster.incrBy(Constants.REDIS_PREFIX + key, delta);
    }

    @Override
    public long decrBy(String key, long delta) {
        return jedisCluster.decrBy(Constants.REDIS_PREFIX + key, delta);
    }

    @Override
    public Long hSet(String key, String filed, String value) {
        return jedisCluster.hset(key, filed, value);
    }

    @Override
    public Long hincrBy(String key, String filed, Long value) {
        return jedisCluster.hincrBy(key, filed, value);
    }

    @Override
    public Map<String, String> hgetAll(String key) {
        return jedisCluster.hgetAll(key);
    }

    @Override
    public Set<String> hkeys(String key) {
        return jedisCluster.hkeys(key);
    }

    @Override
    public long hdel(String key, String... fields) {
        return jedisCluster.hdel(key, fields);
    }

    @Override
    public List<String> hmget(String key, String... fields) {

        return jedisCluster.hmget(key, fields);
    }

    @Override
    public Tuple zPopMin(String key) {
        return jedisCluster.zpopmin(Constants.REDIS_PREFIX + key);
    }

    @Override
    public long zAdd(String key, long score, String value) {
        return jedisCluster.zadd(Constants.REDIS_PREFIX + key, score, value);
    }

    @Override
    public Double zScore(String key, String value) {
        return jedisCluster.zscore(Constants.REDIS_PREFIX + key, value);
    }

    @Override
    public <T> T remember(String key, int seconds, Class<T> classOfType, ICacheResolver<T> resolver) throws Exception {
        if (null == resolver) {
            return null;
        }
        T cacheObject = get(key, classOfType);
        if (null != cacheObject) {
            return cacheObject;
        }
        cacheObject = resolver.resolve();
        if (null == cacheObject) {
            return null;
        }
        set(key, cacheObject, seconds);
        return cacheObject;
    }

    @Override
    public <T> List<T> rememberList(String key, int seconds, Class<T> classOfType, ICacheResolver<List<T>> resolver) throws Exception {
        if (null == resolver) {
            return null;
        }
        List<T> tList = getList(key, classOfType);
        if (!tList.isEmpty()) {
            return tList;
        }
        tList = resolver.resolve();
        if (null == tList) {
            return null;
        }
        set(key, tList, seconds);
        return tList;
    }

    @Override
    public <T> T forever(String key, Class<T> classOfType, ICacheResolver<T> resolver) throws Exception {
        return remember(key, 0, classOfType, resolver);
    }

    @Override
    public <T> List<T> foreverList(String key, Class<T> classOfType, ICacheResolver<List<T>> resolver) throws Exception {
        return rememberList(key, 0, classOfType, resolver);
    }

    @Override
    public long timestamp() {
        Object timestamp = this.jedisCluster.eval("return redis.call('time')[1]", "");
        return Long.parseLong(timestamp.toString());
    }

    @Override
    public long bitcount(String key) {
        return jedisCluster.bitcount(Constants.REDIS_PREFIX + key);
    }

    @Override
    public boolean setbit(String key, long offset, String value) {
        return jedisCluster.setbit(Constants.REDIS_PREFIX + key, offset, value);
    }

    @Override
    public boolean getbit(String key, long offset) {
        return jedisCluster.getbit(Constants.REDIS_PREFIX + key, offset);
    }

    @Override
    public String setnx(String key, String value, int seconds) {
        return jedisCluster.set(Constants.REDIS_PREFIX + key, value, new SetParams().nx().ex(seconds));
    }

}
