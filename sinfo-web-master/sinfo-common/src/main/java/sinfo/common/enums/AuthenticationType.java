package sinfo.common.enums;


public enum AuthenticationType {
    Client("client", "H5页面访问"),
    Server("server", "服务器访问");

    private String type;
    private String name;

    AuthenticationType(String type, String name) {
        this.type = type;
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public static String typeOf(String type) {
        AuthenticationType[] values = values();
        for (int i = 0; i < values.length; i++) {
            AuthenticationType authenticationType = values[i];
            if (authenticationType.getType().equals(type)) {
                return type;
            }
        }
        return null;
    }
}
