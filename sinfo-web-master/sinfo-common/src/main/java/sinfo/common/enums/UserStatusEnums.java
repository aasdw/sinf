package sinfo.common.enums;

public enum UserStatusEnums {
    Unknown(-1, "未知"),
    NotSigning(0, "未签约"),
    YetSigning(1, "已签约"),
    SigningFail(2, "签约失败");

    private final int code;
    private final String name;

    UserStatusEnums(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static UserStatusEnums valueOf(Integer code) {
        for (UserStatusEnums type : UserStatusEnums.values()) {
            if (type.getCode() == code) {
                return type;
            }
        }
        return Unknown;
    }
}