package sinfo.common.enums;

public enum LoginTypeEnums {
    Unknown(0, "未知"),
    Password(1, "密码登录"),
    Captcha(2, "验证码登录");
    private final int code;
    private final String name;

    LoginTypeEnums(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static LoginTypeEnums valueOf(Integer code) {
        for (LoginTypeEnums type : LoginTypeEnums.values()) {
            if (type.getCode() == code) {
                return type;
            }
        }
        return Unknown;
    }
}