package sinfo.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 只适合短暂缓存使用
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Cacheable {
    /**
     * 缓存名字
     * @return
     */
    String[] name() default "";

    /**
     * 缓存时间（秒）
     * @return
     */
    int expireTime() default 3;
}
