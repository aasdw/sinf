package sinfo.common.exceptions;

public class RechargeException extends BaseException {

    public static final String MESSAGE = "充值失败";

    public RechargeException() {
        super(MESSAGE);
    }
    public RechargeException(String message) {
        super(message);
    }
}
