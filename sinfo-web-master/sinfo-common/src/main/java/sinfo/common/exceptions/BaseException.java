package sinfo.common.exceptions;

public class BaseException extends Exception {
    /**
     * 错误信息
     */
    private String message = "";
    /**
     * 其它信息
     */
    private String data = "";

    public String getData() {
        return data;
    }

    public BaseException() {
        super();
    }

    public BaseException(String message) {
        super(message);
        this.message = message;
    }

    public BaseException(String message, String data) {
        super(message);
        this.message = message;
        this.data = data;
    }
}
