package sinfo.common.exceptions;

public class MaxGroupException extends BaseException {
    public MaxGroupException(String message) {
        super(message);
    }
}
