package sinfo.common.exceptions;

public class NotLoginException extends Exception {

    /**
     * 错误信息
     */
    private String message = "";
    /**
     * 其它信息
     */
    private String data = "";

    public String getData() {
        return data;
    }

    public NotLoginException() {
        super();
    }

    public NotLoginException(String message) {
        super(message);
        this.message = message;
    }

    public NotLoginException(String message, String data) {
        super(message);
        this.message = message;
        this.data = data;
    }
}
