package sinfo.common.exceptions;

public class RequestNotAllowedException extends Exception {

    public RequestNotAllowedException() {
    }

    public RequestNotAllowedException(String message) {
        super(message);
    }
}
