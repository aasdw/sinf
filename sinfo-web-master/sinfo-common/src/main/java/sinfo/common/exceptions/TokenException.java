package sinfo.common.exceptions;

public class TokenException extends Exception {
    /**
     * 错误信息
     */
    private String message = "";
    /**
     * 其它信息
     */
    private String data = "";

    public String getData() {
        return data;
    }

    public TokenException() {
        super();
    }

    public TokenException(String message) {
        super(message);
        this.message = message;
    }

    public TokenException(String message, String data) {
        super(message);
        this.message = message;
        this.data = data;
    }
}
