package sinfo.common.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordUtil {

    private static BCryptPasswordEncoder passwordEncoder;

    private static final int SALT_OFFSET = 5;

    static {
        passwordEncoder = new BCryptPasswordEncoder();
    }

    private static String buildRawPassword(String password, String salt) {
        if (!ValidateUtil.isMd5Hash(password)) {
            throw new IllegalArgumentException("Password is not a md5 hash.");
        }
        StringBuilder builder = new StringBuilder(password + salt);
        builder.insert(SALT_OFFSET, salt);
        System.out.println("==>" + builder.toString());
        return builder.toString();
    }

    public static boolean matches(String password, String salt, String encodedPassword) {
        return passwordEncoder.matches(buildRawPassword(password, salt), encodedPassword);
    }

    public static String encode(String password, String salt) {
        return passwordEncoder.encode(buildRawPassword(password, salt));
    }

}
