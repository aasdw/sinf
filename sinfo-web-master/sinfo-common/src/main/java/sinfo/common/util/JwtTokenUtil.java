package sinfo.common.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;
import sinfo.common.constant.Constants;
import sinfo.common.model.auth.TokenModel;

import java.util.Date;

public class JwtTokenUtil {

    private static Algorithm ALGORITHM = Algorithm.HMAC256(Constants.JWT_TOKEN_SECRET);
    private static JWTVerifier jwtVerifier;

    static {
        jwtVerifier = JWT.require(ALGORITHM)
                .withIssuer(Constants.JWT_TOKEN_ISSUER)
                .build();
    }

    public static String createToken(String memberId, String token, int deviceType) {
        return JWT.create()
                .withClaim("id", memberId)
                .withClaim("token", token)
                .withClaim("deviceType", deviceType)
                .withIssuer(Constants.JWT_TOKEN_ISSUER)
                .withAudience(memberId)
                .withIssuedAt(new Date())
                .sign(ALGORITHM);
    }

    public static String createToken(String userId, String token, String phone) {
        return JWT.create()
                .withClaim("id", userId)
                .withClaim("token", token)
                .withClaim("phone", phone)
                .withIssuer(Constants.JWT_TOKEN_ISSUER)
                .withAudience(userId)
                .withIssuedAt(new Date())
                .sign(ALGORITHM);
    }

    public static TokenModel getToken(String authentication) {
        if (StringUtils.isEmpty(authentication)) {
            return null;
        }
        DecodedJWT decode = JwtTokenUtil.checkToken(authentication);
        if (null == decode) {
            return null;
        }
        if (CollectionUtils.isEmpty(decode.getAudience())) {
            return null;
        }
        if (decode.getIssuedAt().after(new Date())) {
            return null;
        }
        String memberId = decode.getClaim("id").asString();
        String token = decode.getClaim("token").asString();
        if (StringUtils.isEmpty(memberId) || StringUtils.isEmpty(token) || !decode.getAudience().contains(memberId)) {
            return null;
        }
        String phone = decode.getClaim("phone").asString();
        if (StringUtils.isEmpty(phone)) {
            return null;
        }
        return new TokenModel(memberId, token, phone);
    }

    public static DecodedJWT checkToken(String token) {
        try {
            return jwtVerifier.verify(token);
        } catch (Exception e) {
            return null;
        }
    }

}
