package sinfo.common.util;

public class WxmtLoginRequest {

    /**
     * memberId
     */
    //@NotBlank(message = "memberId不能为空")
    private String memberId;

    /**
     * appId
     */
    //@NotBlank(message = "appId不能为空")
    private String appId;

    /**
     * secret
     */
    //@NotBlank(message = "secret不能为空")
    private String secret;

    private String sign;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
