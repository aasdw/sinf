package sinfo.common.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import sinfo.common.constant.Constants;
import sinfo.util.util.AESUtils;
import sinfo.util.util.JacksonUtil;
import sinfo.util.util.MD5Util;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import java.security.SecureRandom;
import java.util.*;

public class JwtServerTokenUtil {
    private final static Logger logger = LoggerFactory.getLogger(JwtServerTokenUtil.class);

    private static Algorithm ALGORITHM = Algorithm.HMAC256(Constants.JWT_SERVER_TOKEN_SECRET);
    private static JWTVerifier jwtVerifier;

    static {
        jwtVerifier = JWT.require(ALGORITHM)
                .withIssuer(Constants.JWT_TOKEN_ISSUER)
                // 允许 Token 生成时间有 60 秒的误差
                .acceptLeeway(60000)
                .build();
    }

    public static String getTokenKey(String appId) {
        return Constants.SERVER_AUTH_TOKEN + appId;
    }

    public static String createToken(String appId) {
        String random = getGUID();
        String token = JWT.create()
                .withClaim("appId", appId)
                .withClaim("random", random)
                .withIssuer(Constants.JWT_TOKEN_ISSUER)
                .withAudience(appId)
                .withIssuedAt(new Date())
                .sign(ALGORITHM);
        return token;
    }

    public static void main(String[] args) throws Exception {
        Map<String,String> map = new HashMap<>();
        map.put("appId","04e6abb0c8cc448a91061e49ed50a4eb");
        map.put("secret","/k3mwcSraFGA3F5NLd6gJg==");
//        map.put("memberId","a14b297f45ac4e53aff69b0a76caf008");
        String json = JacksonUtil.toJson(map);
        System.out.println(json);
        String encrypt = AESUtils.encrypt(json);
        System.out.println(encrypt);
//        System.out.println(URLEncoder.encode(encrypt,"utf-8"));
        System.out.println(MD5Util.md5(json));

    }

    public static DecodedJWT checkToken(String token) {
        try {
            return jwtVerifier.verify(token);
        } catch (Exception e) {
            return null;
        }
    }

    public static DecodedJWT getServerToken(String authentication) {
        DecodedJWT decodedJWT = null;
        try {
            if (StringUtils.isEmpty(authentication)) {
                return null;
            }
            decodedJWT = JwtServerTokenUtil.checkToken(authentication);
            if (null == decodedJWT) {
                return null;
            }
            if (CollectionUtils.isEmpty(decodedJWT.getAudience())) {
                return null;
            }
        } catch (Exception e) {
            logger.error("解析token异常", e.getMessage());
        }
        return decodedJWT;
    }

    public static String getGUID() {
        StringBuilder uid = new StringBuilder();
        //产生16位的强随机数
        Random rd = new SecureRandom();
        for (int i = 0; i < 16; i++) {
            //产生0-2的3位随机数
            int type = rd.nextInt(3);
            switch (type) {
                case 0:
                    //0-9的随机数
                    uid.append(rd.nextInt(10));
                	/*int random = ThreadLocalRandom.current().ints(0, 10)
                	.distinct().limit(1).findFirst().getAsInt();*/
                    break;
                case 1:
                    //ASCII在65-90之间为大写,获取大写随机
                    uid.append((char) (rd.nextInt(25) + 65));
                    break;
                case 2:
                    //ASCII在97-122之间为小写，获取小写随机
                    uid.append((char) (rd.nextInt(25) + 97));
                    break;
                default:
                    break;
            }
        }
        return uid.toString();
    }

}
