package sinfo.common.util;


import org.springframework.util.StringUtils;

public class HeaderUtils {

    public HeaderUtils() {
    }

    public static String getValue(String key, boolean needDecode) {
        String str = HttpContextUtils.getHttpServletRequest().getHeader(key);

        if (!StringUtils.isEmpty(str) ) {
            return needDecode ? CodeUnEnUtils.deCode(str) : str;
        } else {
            return "";
        }
    }

    public static String getMemberId() {
        String memberId = HttpContextUtils.getHttpServletRequest().getHeader("memberId");
        return memberId;
    }

    public static String getOpenid() {
        String openid = HttpContextUtils.getHttpServletRequest().getHeader("openid");
        return openid;
    }
}
