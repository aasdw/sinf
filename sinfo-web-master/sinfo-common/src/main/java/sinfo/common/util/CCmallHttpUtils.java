package sinfo.common.util;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class CCmallHttpUtils {

    private static Logger logger = LoggerFactory.getLogger(CCmallHttpUtils.class);

    //超时时间
    private static final int timeOut = 20000;

    private static CloseableHttpClient httpClient = null;

    private final static Object syncLock = new Object();

    /**
     * 获取签名
     *
     * @param mapResult
     * @return
     */
    /*public static String getSign(Map<String, String> mapResult, String memberId) {
        //生成的待签名字符串
        String waitingForSign = new String();
        Iterator<Map.Entry<String, String>> it = mapResult.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> entry = it.next();
            if (org.apache.commons.lang3.StringUtils.isNotBlank(entry.getValue())) { // 空值不参与签名
                waitingForSign = waitingForSign.concat(entry.getKey() + "=" + entry.getValue() + "&");
            }
        }
        logger.info("waitingForSign:" + waitingForSign);
        waitingForSign = waitingForSign.concat("signKey=" + memberId);
        String sign = DigestUtils.md5Hex(waitingForSign).toLowerCase();
        return sign;
    }*/


    public static String getSign(String text,String key) throws Exception {
        System.out.println(text);
        //加密后的字符串
        String sign = DigestUtils.md5Hex(text + key);
        return sign;
    }

    //参数排序
    public static Map<String, String> sortMapByKey(Map<String, String> map) {
        if (map == null || map.isEmpty()) {
            return null;
        }
        Map<String, String> sortMap = new TreeMap<String, String>(
                new MapKeyComparator());
        sortMap.putAll(map);
        return sortMap;
    }

    private static void setPostParams(HttpPost httpost, Map<String, String> params) {
        String json = "{";
        Set<String> keySet = params.keySet();
        for (String key : keySet) {
            json = json.concat("\"" + key + "\":\"" + params.get(key) + "\",");
        }
        json = json.substring(0, json.length() - 1);
        json = json.concat("}");
        logger.info("json:" + json);
        try {
            StringEntity stringEntity = new StringEntity(json, "UTF-8");
            logger.info("stringEntity:" + stringEntity);
            httpost.addHeader("Content-Type", "application/json");
            httpost.setEntity(stringEntity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * POST请求URL获取内容
     */
    public static String post(String url, Map params) throws IOException {
        HttpPost httppost = new HttpPost(url);
        config(httppost);
        setPostParams(httppost, params);
        CloseableHttpResponse response = null;
        try {
            response = getHttpClient(url).execute(httppost,
                    HttpClientContext.create());
            HttpEntity entity = response.getEntity();
            String result = EntityUtils.toString(entity, "utf-8");
            EntityUtils.consume(entity);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (response != null)
                    response.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * 获取HttpClient对象
     */
    public static CloseableHttpClient getHttpClient(String url) {
        String hostname = url.split("/")[2];
        int port = 80;
        if (hostname.contains(":")) {
            String[] arr = hostname.split(":");
            hostname = arr[0];
            port = Integer.parseInt(arr[1]);
        }
        if (httpClient == null) {
            synchronized (syncLock) {
                if (httpClient == null) {
                    //httpClient = HttpClientUtil.createHttpClient(hostname, port);
                }
            }
        }
        return httpClient;
    }

    private static void config(HttpRequestBase httpRequestBase) {
        // 配置请求的超时设置
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectionRequestTimeout(timeOut)
                .setConnectTimeout(timeOut).setSocketTimeout(timeOut).build();
        httpRequestBase.setConfig(requestConfig);
    }
}
