package sinfo.common.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class CodeUnEnUtils {
    /**
     * URI中文解义
     * @author OprCalf
     * @param encodeText
     * @return String
     */
    public static String deCode(String encodeText) {
        String returnText = "";
        try {
            returnText = URLDecoder.decode(encodeText, "UTF-8");
            return returnText;
        }
        catch (final UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return returnText;
    }
}
