package sinfo.common.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import sinfo.common.constant.Constants;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import java.security.SecureRandom;
import java.util.Date;
import java.util.Random;

public class JwtClientTokenUtil {
    private final static Logger logger = LoggerFactory.getLogger(JwtClientTokenUtil.class);

    private static Algorithm ALGORITHM = Algorithm.HMAC256(Constants.JWT_CLIENT_TOKEN_SECRET);
    private static JWTVerifier jwtVerifier;

    public static String getTokenKey(String appId, String memberId) {
        return Constants.CLIENT_AUTH_TOKEN + appId + ":" + memberId;
    }

    static {
        jwtVerifier = JWT.require(ALGORITHM)
                .withIssuer(Constants.JWT_TOKEN_ISSUER)
                // 允许 Token 生成时间有 60 秒的误差
                .acceptLeeway(60000)
                .build();
    }

    public static String createToken(String memberId, String appId) {
        String random = getGUID();
        return JWT.create()
                .withClaim("memberId", memberId)
                .withClaim("appId", appId)
                .withClaim("random", random)
                .withIssuer(Constants.JWT_TOKEN_ISSUER)
                .withAudience(appId)
                .withIssuedAt(new Date())
                .sign(ALGORITHM);
    }

    public static DecodedJWT checkToken(String token) {
        try {
            return jwtVerifier.verify(token);
        } catch (Exception e) {
            return null;
        }
    }

    public static DecodedJWT getClientToken(String authentication) {
        DecodedJWT decodedJWT = null;
        try {
            if (StringUtils.isEmpty(authentication)) {
                return null;
            }
            decodedJWT = JwtClientTokenUtil.checkToken(authentication);
            if (null == decodedJWT) {
                return null;
            }
            if (CollectionUtils.isEmpty(decodedJWT.getAudience())) {
                return null;
            }
        } catch (Exception e) {
            logger.error("解析token异常", e.getMessage());
        }
        return decodedJWT;
    }

    public static String getGUID() {
        StringBuilder uid = new StringBuilder();
        //产生16位的强随机数
        Random rd = new SecureRandom();
        for (int i = 0; i < 32; i++) {
            //产生0-2的3位随机数
            int type = rd.nextInt(3);
            switch (type) {
                case 0:
                    //0-9的随机数
                    uid.append(rd.nextInt(10));
                    break;
                case 1:
                    //ASCII在65-90之间为大写,获取大写随机
                    uid.append((char) (rd.nextInt(25) + 65));
                    break;
                case 2:
                    //ASCII在97-122之间为小写，获取小写随机
                    uid.append((char) (rd.nextInt(25) + 97));
                    break;
                default:
                    break;
            }
        }
        return uid.toString();
    }

}
