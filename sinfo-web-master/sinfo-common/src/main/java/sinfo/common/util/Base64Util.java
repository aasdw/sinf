package sinfo.common.util;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;


public class Base64Util {

    public static void main(String[] args) throws Exception {
        String str = "https://www.baidu.com/";
        //加密
        System.out.println(encryptBASE64(str.getBytes()));

        //解密
        System.out.println(new String(decryptBASE64("aHR0cHM6Ly93d3cuYmFpZHUuY29tLw==")));

    }

    /**
     * BASE64解密
     *
     * @throws Exception
     */
    public static byte[] decryptBASE64(String key) throws Exception {
        return (new BASE64Decoder()).decodeBuffer(key);
    }

    /**
     * BASE64加密
     */
    public static String encryptBASE64(byte[] key) throws Exception {
        return (new BASE64Encoder()).encodeBuffer(key);
    }

}
