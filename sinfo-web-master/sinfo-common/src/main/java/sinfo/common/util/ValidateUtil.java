package sinfo.common.util;

import org.apache.commons.lang3.StringUtils;

import java.util.UUID;
import java.util.regex.Pattern;

public class ValidateUtil {

    public static boolean isNumeric(String value) {
        return StringUtils.isNumeric(value);
    }

    public static boolean isPhone(String phone) {
        if (StringUtils.isEmpty(phone)) {
            return false;
        }
        String regex = "([+\\-])?(297|93|244|1264|355|376|971|54|374|1684|1268|61|43|994|257|32|229|226|880|359|973|1242|387|590|375|501|1441|591|55|1246|673|975|267|236|1|41|56|86|225|237|243|242|682|57|269|238|506|53|5999|1345|357|420|49|253|1767|45|1809|1829|1849|213|593|20|291|212|34|372|251|358|679|500|33|298|691|241|995|233|350|224|220|245|240|30|1473|299|502|594|1671|592|852|504|385|509|36|62|91|246|353|98|964|354|972|39|1876|44|962|81|76|77|254|996|855|686|1869|82|383|965|856|961|231|218|1758|423|94|266|370|352|371|853|377|373|261|960|52|692|389|223|356|95|382|976|1670|258|222|1664|596|230|265|60|264|687|227|672|234|505|683|31|47|977|674|64|968|92|507|51|63|680|675|48|1787|1939|850|351|595|970|689|974|262|40|7|250|966|249|221|65|4779|677|232|503|378|252|508|381|211|239|597|421|386|46|268|1721|248|963|1649|235|228|66|992|690|993|670|676|1868|216|90|688|886|255|256|380|598|998|379|1784|58|1284|1340|84|678|681|685|967|27|260|263)(9[976]\\d|8[987530]\\d|6[987]\\d|5[90]\\d|42\\d|3[875]\\d|2[98654321]\\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\\d{6,13}$";
        return Pattern.matches(regex, phone);
    }

    public static boolean isChinesePhone(String phone) {
        if (StringUtils.isEmpty(phone)) {
            return false;
        }
        String regex = "((\\+|00)?86)?1[3-9][0-9]{9}$";
        return Pattern.matches(regex, phone);
    }

    public static boolean isUUID(String uuid) {
        if (StringUtils.isEmpty(uuid)) {
            return false;
        }
        if (!(uuid.length() == 32 || uuid.length() == 36)) {
            return false;
        }
        try {
            if (uuid.length() == 32) {
                uuid = uuid.substring(0, 8) +
                        "-" +
                        uuid.substring(8, 12) +
                        "-" +
                        uuid.substring(12, 16) +
                        "-" +
                        uuid.substring(16, 20) +
                        "-" +
                        uuid.substring(20, 32);
            }
            UUID.fromString(uuid);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }


    public static boolean isMd5Hash(String value) {
        if (StringUtils.isEmpty(value)) {
            return false;
        }

        return Pattern.matches("[0-9a-fA-F]{32}", value);
    }

    public static boolean isEmail(String email) {
        if (StringUtils.isEmpty(email)) {
            return false;
        }
        return Pattern.matches("\\w+@\\w+\\.[a-z]+(\\.[a-z]+)?", email);
    }

    public static boolean isCaptcha(String captcha) {
        if (StringUtils.isEmpty(captcha)) {
            return false;
        }
        return true;
    }

    public static boolean twoDecimal(String price) {
        if (StringUtils.isEmpty(price)) {
            return false;
        }
        return Pattern.matches("^(([1-9]{1}\\d*)|(0{1}))(\\.\\d{1,2})?$", price);
    }

    public static boolean isURL(String url) {
        if (StringUtils.isEmpty(url)) {
            return false;
        }
        return url.startsWith("https://") || url.startsWith("http://");
    }

}
