package sinfo.common.util;

import org.springframework.util.Assert;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HttpContextUtils {
    public HttpContextUtils() {
    }

    public static HttpServletRequest getHttpServletRequest() {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        Assert.notNull(requestAttributes,"获取不到当前的requestAttributes:"+new Object[0]);
        return requestAttributes.getRequest();

    }

    public static HttpServletResponse getHttpServletResponse() {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        Assert.notNull(requestAttributes,"获取不到当前的requestAttributes:"+new Object[0]);
        return requestAttributes.getResponse();

    }
}
