package sinfo.common.util;

import sinfo.common.constant.Constants;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class SessionUtil {
    public static String getRequestAttribute(String attributeName) {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (null == requestAttributes) {
            return null;
        }
        return (String) requestAttributes.getAttribute(attributeName, RequestAttributes.SCOPE_REQUEST);
    }

    public static void setCurrentCustomerId(String customerId){
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (null == requestAttributes) {
            return;
        }
        requestAttributes.setAttribute(Constants.REQUEST_CUSTOMER_ID, customerId, RequestAttributes.SCOPE_REQUEST);
    }

    public static String getCurrentCustomerId() {
        return getRequestAttribute(Constants.REQUEST_CUSTOMER_ID);
    }



    public static void setCurrentMemberId(String memberId){
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (null == requestAttributes) {
            return;
        }
        requestAttributes.setAttribute(Constants.REQUEST_MEMBER_ID, memberId, RequestAttributes.SCOPE_REQUEST);
    }

    public static String getCurrentMemberId() {
        return getRequestAttribute(Constants.REQUEST_MEMBER_ID);
    }
}
