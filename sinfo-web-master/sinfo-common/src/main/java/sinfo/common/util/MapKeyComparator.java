package sinfo.common.util;

import java.util.Comparator;

/**
 * Created by Administrator on 2019/2/12 0012.
 */
public class MapKeyComparator implements Comparator<String> {
    @Override
    public int compare(String str1, String str2) {

        return str1.compareTo(str2);
    }
}
