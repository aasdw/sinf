package sinfo.common.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.NoHandlerFoundException;
import sinfo.common.exceptions.NotLoginException;
import sinfo.common.exceptions.TokenException;
import sinfo.util.message.BaseResponse;
import sinfo.util.util.ResponseUtil;

import javax.annotation.Resource;
import javax.servlet.MultipartConfigElement;
import javax.validation.ConstraintViolationException;
import java.sql.SQLException;

@ControllerAdvice
public class GlobalExceptionResolverHandler {

    private final Logger logger = LoggerFactory.getLogger(GlobalExceptionResolverHandler.class);

    @Resource
    MultipartConfigElement multipartConfigElement;

    @ExceptionHandler(Throwable.class)
    @ResponseBody
    public ResponseEntity<BaseResponse> handleException(Exception exception) {
        BaseResponse response = resolveException(exception);
        return ResponseEntity.status(HttpStatus.OK.value())
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    private BaseResponse resolveException(Exception exception) {
        if (exception instanceof NoHandlerFoundException) {
            return ResponseUtil.error(HttpStatus.NOT_FOUND.getReasonPhrase(), HttpStatus.NOT_FOUND.value());
        } else if (exception instanceof HttpRequestMethodNotSupportedException) {
            return ResponseUtil.error(HttpStatus.METHOD_NOT_ALLOWED.getReasonPhrase(), HttpStatus.METHOD_NOT_ALLOWED.value());
        } else if (exception instanceof IllegalArgumentException) {
            return ResponseUtil.error(exception.getMessage(), HttpStatus.NOT_ACCEPTABLE.value());
        } else if (exception instanceof HttpMediaTypeNotSupportedException) {
            return ResponseUtil.error(HttpStatus.UNSUPPORTED_MEDIA_TYPE.getReasonPhrase(), HttpStatus.UNSUPPORTED_MEDIA_TYPE.value());
        } else if (exception instanceof SQLException || exception instanceof DataAccessException) {
            logger.error("SQL 执行错误", exception);
            return ResponseUtil.error("数据出错", HttpStatus.INTERNAL_SERVER_ERROR.value());
        } else if (exception instanceof MaxUploadSizeExceededException) {
            return ResponseUtil.error("超出最大上传限制：10M", HttpStatus.BAD_REQUEST.value());
        } else if (exception instanceof RuntimeException) {
            logger.error("运行时错误", exception);
            return ResponseUtil.error(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(), HttpStatus.INTERNAL_SERVER_ERROR.value());
        } else if (exception instanceof BindException) {
            BindingResult bindingResult = ((BindException) exception).getBindingResult();
            return ResponseUtil.error(bindingResult.getFieldError().getDefaultMessage());
        } else if (exception instanceof MethodArgumentNotValidException) {
            BindingResult bindingResult = ((MethodArgumentNotValidException) exception).getBindingResult();
            return ResponseUtil.error(bindingResult.getFieldError().getDefaultMessage(), HttpStatus.BAD_REQUEST.value());
        } else if (exception instanceof ConstraintViolationException) {
            return ResponseUtil.error(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(), HttpStatus.INTERNAL_SERVER_ERROR.value());
        } else if (exception instanceof NumberFormatException) {
            return ResponseUtil.error(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(), HttpStatus.INTERNAL_SERVER_ERROR.value());
        } else if(exception instanceof TokenException){
            return ResponseUtil.error(exception.getMessage(), HttpStatus.GONE.value());
        } else if (exception instanceof NotLoginException) {
            return ResponseUtil.error(exception.getMessage(), HttpStatus.LENGTH_REQUIRED.value());
        } else {
            logger.error("出错了", exception);
            return ResponseUtil.error(exception.getMessage(), HttpStatus.BAD_REQUEST.value());
        }
    }
}
