package sinfo.common.request;

public interface IHttpRequest {

    void validate() throws IllegalArgumentException;
}
