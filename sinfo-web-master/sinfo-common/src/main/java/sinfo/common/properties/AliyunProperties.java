package sinfo.common.properties;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AliyunProperties {

    @Value("${aliyun.access-key-id}")
    private String accessKeyId;
    @Value("${aliyun.access-key-secret}")
    private String accessKeySecret;

    @Value("${aliyun.oss.endpoint}")
    private String ossEndpoint;
    @Value("${aliyun.oss.bucket}")
    private String ossBucket;
    @Value("${aliyun.oss.folder}")
    private String ossFolder;

    @Value("${aliyun.log.endpoint}")
    private String logEndpoint;
    @Value("${aliyun.log.project}")
    private String logProject;
    @Value("${aliyun.log.store}")
    private String logStore;

    @Value("${aliyun.cdn.prefix}")
    private String cdnPrefix;

    @Value("${aliyun.sms.sign-name}")
    private String smsSignName;
    @Value("${aliyun.sms.template.auth}")
    private String smsAuthTemplate;
    @Value("${aliyun.sms.template.register}")
    private String smsRegTemplate;
    @Value("${aliyun.sms.template.modify}")
    private String smsModifyTemplate;
    @Value("${aliyun.sms.template.international.auth}")
    private String smsInternationalAuthTemplate;
    @Value("${aliyun.sms.template.international.register}")
    private String smsInternationalRegTemplate;
    @Value("${aliyun.sms.template.international.modify}")
    private String smsInternationalModifyTemplate;

    @Value("${aliyun.express.query.appCode}")
    private String expressQueryAppCode;
    @Value("${aliyun.express.query.url}")
    private String expressQueryUrl;

    @Value("${aliyun.idcard.verified.app-code}")
    private String idCardVerifiedAppCode;
    @Value("${aliyun.idcard.verified.url}")
    private String idCardVerifiedUrl;

    @Value("${aliyun.bank.verified.app-code}")
    private String bankVerifiedAppCode;
    @Value("${aliyun.bank.verified.url}")
    private String bankVerifiedUrl;
    @Value("${aliyun.bank4.verified.url}")
    private String bank4VerifiedUrl;

    @Value("${aliyun.scm.app-key}")
    private String scmAppKey;
    @Value("${aliyun.scm.app-secret}")
    private String scmAppSecret;
    @Value("${aliyun.scm.access-token}")
    private String scmAccessToken;


    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    public String getAccessKeySecret() {
        return accessKeySecret;
    }

    public void setAccessKeySecret(String accessKeySecret) {
        this.accessKeySecret = accessKeySecret;
    }

    public String getOssEndpoint() {
        return ossEndpoint;
    }

    public void setOssEndpoint(String ossEndpoint) {
        this.ossEndpoint = ossEndpoint;
    }

    public String getOssBucket() {
        return ossBucket;
    }

    public void setOssBucket(String ossBucket) {
        this.ossBucket = ossBucket;
    }

    public String getLogEndpoint() {
        return logEndpoint;
    }

    public String getOssFolder() {
        return ossFolder;
    }

    public void setOssFolder(String ossFolder) {
        this.ossFolder = ossFolder;
    }

    public void setLogEndpoint(String logEndpoint) {
        this.logEndpoint = logEndpoint;
    }

    public String getLogProject() {
        return logProject;
    }

    public void setLogProject(String logProject) {
        this.logProject = logProject;
    }

    public String getLogStore() {
        return logStore;
    }

    public void setLogStore(String logStore) {
        this.logStore = logStore;
    }

    public String getCdnPrefix() {
        if (StringUtils.isEmpty(cdnPrefix)) {
            return "";
        }
        if (!cdnPrefix.endsWith("/")) {
            cdnPrefix += "/";
        }
        return cdnPrefix;
    }

    public void setCdnPrefix(String cdnPrefix) {
        this.cdnPrefix = cdnPrefix;
    }

    public String getSmsSignName() {
        return smsSignName;
    }

    public void setSmsSignName(String smsSignName) {
        this.smsSignName = smsSignName;
    }

    public String getSmsAuthTemplate() {
        return smsAuthTemplate;
    }

    public void setSmsAuthTemplate(String smsAuthTemplate) {
        this.smsAuthTemplate = smsAuthTemplate;
    }

    public String getSmsRegTemplate() {
        return smsRegTemplate;
    }

    public void setSmsRegTemplate(String smsRegTemplate) {
        this.smsRegTemplate = smsRegTemplate;
    }

    public String getSmsModifyTemplate() {
        return smsModifyTemplate;
    }

    public void setSmsModifyTemplate(String smsModifyTemplate) {
        this.smsModifyTemplate = smsModifyTemplate;
    }

    public String getSmsInternationalAuthTemplate() {
        return smsInternationalAuthTemplate;
    }

    public void setSmsInternationalAuthTemplate(String smsInternationalAuthTemplate) {
        this.smsInternationalAuthTemplate = smsInternationalAuthTemplate;
    }

    public String getSmsInternationalRegTemplate() {
        return smsInternationalRegTemplate;
    }

    public void setSmsInternationalRegTemplate(String smsInternationalRegTemplate) {
        this.smsInternationalRegTemplate = smsInternationalRegTemplate;
    }

    public String getSmsInternationalModifyTemplate() {
        return smsInternationalModifyTemplate;
    }

    public void setSmsInternationalModifyTemplate(String smsInternationalModifyTemplate) {
        this.smsInternationalModifyTemplate = smsInternationalModifyTemplate;
    }

    public String getExpressQueryAppCode() {
        return expressQueryAppCode;
    }

    public void setExpressQueryAppCode(String expressQueryAppCode) {
        this.expressQueryAppCode = expressQueryAppCode;
    }

    public String getExpressQueryUrl() {
        return expressQueryUrl;
    }

    public void setExpressQueryUrl(String expressQueryUrl) {
        this.expressQueryUrl = expressQueryUrl;
    }

    public String getIdCardVerifiedAppCode() {
        return idCardVerifiedAppCode;
    }

    public void setIdCardVerifiedAppCode(String idCardVerifiedAppCode) {
        this.idCardVerifiedAppCode = idCardVerifiedAppCode;
    }

    public String getIdCardVerifiedUrl() {
        return idCardVerifiedUrl;
    }

    public void setIdCardVerifiedUrl(String idCardVerifiedUrl) {
        this.idCardVerifiedUrl = idCardVerifiedUrl;
    }

    public String getBankVerifiedAppCode() {
        return bankVerifiedAppCode;
    }

    public void setBankVerifiedAppCode(String bankVerifiedAppCode) {
        this.bankVerifiedAppCode = bankVerifiedAppCode;
    }

    public String getBankVerifiedUrl() {
        return bankVerifiedUrl;
    }

    public void setBankVerifiedUrl(String bankVerifiedUrl) {
        this.bankVerifiedUrl = bankVerifiedUrl;
    }

    public String getBank4VerifiedUrl() {
        return bank4VerifiedUrl;
    }

    public void setBank4VerifiedUrl(String bank4VerifiedUrl) {
        this.bank4VerifiedUrl = bank4VerifiedUrl;
    }

    public String getScmAppKey() {
        return scmAppKey;
    }

    public void setScmAppKey(String scmAppKey) {
        this.scmAppKey = scmAppKey;
    }

    public String getScmAppSecret() {
        return scmAppSecret;
    }

    public void setScmAppSecret(String scmAppSecret) {
        this.scmAppSecret = scmAppSecret;
    }

    public String getScmAccessToken() {
        return scmAccessToken;
    }

    public void setScmAccessToken(String scmAccessToken) {
        this.scmAccessToken = scmAccessToken;
    }
}
