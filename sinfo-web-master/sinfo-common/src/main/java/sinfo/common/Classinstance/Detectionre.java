package sinfo.common.Classinstance;

import java.util.Date;

public class Detectionre {

    private  Integer  id;
    private  String  score;
    private  Integer type;
    private  String  level;
    private  String  content;
    private  String  creator;
    private  Integer  delete_flag;
    private  Date  create_date;
    private  Date  update_date;



    public void setId(Integer id){

        this.id=id;

    }

    public Integer  getId(){

        return id;

    }



    public void setScore(String score){

        this.score=score;

    }

    public String  getScore(){


        return score;

    }


    public void setType(Integer type){


        this.type=type;

    }

    public Integer  getType(){


        return type;

    }



    public void setLevel(String  level){


        this.level=level;

    }

    public String   getLevel(){


        return level;

    }




    public void  setContent(String  content){


        this.content=content;

    }

    public String   getContent(){


        return content;

    }



    public void  setCreator(String  creator){


        this.creator=creator;

    }

    public String   getCreator(){


        return creator;

    }


    public void  setDelete_flag(Integer  delete_flag){


        this.delete_flag=delete_flag;

    }

    public Integer   getDelete_flag(){


        return delete_flag;

    }




    public void  setCreate_date(Date  create_date){


        this.create_date=create_date;

    }


    public Date   getCreate_date(){


        return create_date;

    }



    public void  setUpdate_date(Date  update_date){


        this.update_date=update_date;

    }


    public Date   getUpdate_date(){


        return update_date;

    }




}
