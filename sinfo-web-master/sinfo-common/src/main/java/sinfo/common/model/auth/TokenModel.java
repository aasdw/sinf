package sinfo.common.model.auth;

public class TokenModel {

    private String id;
    private String token;
    private int deviceType;
    private String phone;

    public TokenModel() {
    }

    public TokenModel(String id, String token) {
        this.id = id;
        this.token = token;
    }

    public TokenModel(String id, String token, int deviceType) {
        this.id = id;
        this.token = token;
        this.deviceType = deviceType;
    }

    public TokenModel(String id, String token, String phone) {
        this.id = id;
        this.token = token;
        this.phone = phone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(int deviceType) {
        this.deviceType = deviceType;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
