package sinfo.common.model.page;


import sinfo.common.pagination.IPagerCallback;
import sinfo.common.pagination.IPagerListCallback;

import java.util.List;

public class PagerBuilder<T, E> {

    private int page;
    private int pageSize;
    private IPagerListCallback<List<T>> listPagerCallback;
    private IPagerCallback<Integer> recordTotalCallback;
    private IPagerCallback<E> exDataCallback;

    public PagerBuilder(int page, int pageSize) {
        this.page = Math.max(1, page);
        this.pageSize = Math.max(1, pageSize);
    }

    public PagerBuilder(String page, String pageSize) {
        this(Integer.parseInt(page), Integer.parseInt(pageSize));
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public IPagerListCallback<List<T>> getListPagerCallback() {
        return listPagerCallback;
    }

    public void setListPagerCallback(IPagerListCallback<List<T>> listPagerCallback) {
        this.listPagerCallback = listPagerCallback;
    }

    public IPagerCallback<Integer> getRecordTotalCallback() {
        return recordTotalCallback;
    }

    public void setRecordTotalCallback(IPagerCallback<Integer> recordTotalCallback) {
        this.recordTotalCallback = recordTotalCallback;
    }

    public IPagerCallback<E> getExDataCallback() {
        return exDataCallback;
    }

    public void setExDataCallback(IPagerCallback<E> exDataCallback) {
        this.exDataCallback = exDataCallback;
    }

    public Pager<T, E> build() throws Exception {
        Pager<T, E> pager = new Pager<>();
        pager.setPage(getPage());
        pager.setPageSize(getPageSize());
        int total = 0;
        if (null != recordTotalCallback) {
            total = recordTotalCallback.resolve();
        }
        if (total > 0) {
            List<T> list = listPagerCallback.resolve((getPage() - 1) * getPageSize(), getPageSize());
            pager.setTotal(total);
            int pageTotal = (int) Math.max(1, Math.ceil(pager.getTotal() * 1.0f / pager.getPageSize()));
            pager.setPageTotal(pageTotal);
            pager.setList(list);
        }
        if (null != exDataCallback) {
            pager.setEx(exDataCallback.resolve());
        }
        return pager;
    }
}
