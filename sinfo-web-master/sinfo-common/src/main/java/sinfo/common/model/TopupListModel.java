package sinfo.common.model;


import java.math.BigDecimal;

public class TopupListModel {

    /**
     * 充值金额
     */
    private BigDecimal topupAmount;

    private String createDate;

    /**
     * 类型
     */
    private Integer type;


    public BigDecimal getTopupAmount() {
        return topupAmount;
    }

    public void setTopupAmount(BigDecimal topupAmount) {
        this.topupAmount = topupAmount;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
