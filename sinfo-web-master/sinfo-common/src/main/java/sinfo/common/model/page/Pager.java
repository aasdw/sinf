package sinfo.common.model.page;

import java.util.List;

public class Pager<T, E> {

    private int page;
    private int pageSize;
    private int pageTotal;
    private int total;
    private List<T> list;
    private E ex;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageTotal() {
        return pageTotal;
    }

    public void setPageTotal(int pageTotal) {
        this.pageTotal = pageTotal;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public E getEx() {
        return ex;
    }

    public void setEx(E ex) {
        this.ex = ex;
    }
}
