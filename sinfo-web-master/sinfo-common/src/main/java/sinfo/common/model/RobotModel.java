package sinfo.common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class RobotModel {
    private Integer jobNumber;
    private String customerId;
    @JsonIgnore
    private String memberId;
    private String deviceId;
    private Integer robotStatus;
    @JsonIgnore
    private String wechatInfoId;
    @JsonIgnore
    private String wechatServerInfo;

    public Integer getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(Integer jobNumber) {
        this.jobNumber = jobNumber;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getRobotStatus() {
        return robotStatus;
    }

    public void setRobotStatus(Integer robotStatus) {
        this.robotStatus = robotStatus;
    }

    public String getWechatInfoId() {
        return wechatInfoId;
    }

    public void setWechatInfoId(String wechatInfoId) {
        this.wechatInfoId = wechatInfoId;
    }

    public String getWechatServerInfo() {
        return wechatServerInfo;
    }

    public void setWechatServerInfo(String wechatServerInfo) {
        this.wechatServerInfo = wechatServerInfo;
    }

}
