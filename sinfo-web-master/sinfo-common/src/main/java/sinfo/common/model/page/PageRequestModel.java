package sinfo.common.model.page;

import org.apache.commons.lang3.StringUtils;
import sinfo.common.request.IHttpRequest;

public class PageRequestModel implements IHttpRequest {

    private String page = "1";
    private String pageSize = "1";

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNum() {
        return Integer.parseInt(page);
    }

    public int getPageSizeNum() {
        return Integer.parseInt(pageSize);
    }

    @Override
    public void validate() throws IllegalArgumentException {
        if (!StringUtils.isEmpty(getPage())) {
            if (!StringUtils.isAlphanumeric(getPage())) {
                throw new IllegalArgumentException("页码格式错误");
            }
            int page = Integer.parseInt(getPage());
            if (page <= 0) {
                throw new IllegalArgumentException("页码格式错误");
            }
        }

        if (!StringUtils.isEmpty(getPageSize())) {
            if (!StringUtils.isAlphanumeric(getPageSize())) {
                throw new IllegalArgumentException("每页数量格式错误");
            }
            int pageSize = Integer.parseInt(getPageSize());
            if (pageSize <= 0 || pageSize > 200) {
                throw new IllegalArgumentException("每页数量格式错误");
            }
        }
    }
}
