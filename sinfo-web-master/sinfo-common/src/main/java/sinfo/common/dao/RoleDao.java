package sinfo.common.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import sinfo.common.bo.RoleBo;
import sinfo.model.entity.RoleEntity;

import java.util.List;

@Mapper
public interface RoleDao extends BaseMapper<RoleEntity> {

    @Select({"<script>",
            " SELECT * FROM roles WHERE delete_flag=0 ",
            "<if test='name != null'> ",
            " AND name LIKE concat('%',#{name,jdbcType=VARCHAR},'%') ",
            "</if>",
            "<if test='status != null'> ",
            "  AND status = #{status} ",
            "</if>",
            " ORDER BY create_date desc limit #{page},#{pageSize}",
            "</script>"})
    List<RoleEntity> selectRoleList(@Param("name") String name, @Param("status") Integer status,
                                    @Param("page") Integer page, @Param("pageSize") Integer pageSize);

    @Select({"<script>",
            " SELECT count(*) FROM roles WHERE delete_flag=0 ",
            "<if test='name != null'> ",
            " AND name LIKE concat('%',#{name,jdbcType=VARCHAR},'%')",
            "</if>",
            "<if test='status != null'> ",
            "  AND status = #{status} ",
            "</if>",
            "</script>"})
    int selectRoleListCount(@Param("name") String name, @Param("status") Integer status);


    @Select(" SELECT count(*) FROM sys_user s  WHERE s.role_id= #{roleId} ")
    int selectSysUserNumber(@Param("roleId") Integer roleId);

    @Insert(" INSERT INTO roles (name, remark) " +
            " VALUES (#{name},#{remark}) ON DUPLICATE KEY UPDATE name =#{name}, remark =#{remark} ")
    int addOrUpdate(RoleBo roleBo);



}
