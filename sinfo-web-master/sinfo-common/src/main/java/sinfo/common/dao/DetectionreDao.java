package sinfo.common.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;
import sinfo.common.Classinstance.Detectionre;
import sinfo.model.entity.DetectionreEntity;


import java.util.List;

@Mapper

public interface    DetectionreDao   extends BaseMapper<DetectionreEntity> {


    @Insert(" INSERT INTO detection_report (score, type,level,content,creator,delete_flag,create_date,update_date) " +
            " VALUES (#{score},#{type},#{level},#{content},#{creator},#{delete_flag},#{create_date},#{update_date})")
    int adddetection(Detectionre detec);

    @Delete("delete from detection_report   where id =#{id}")
    int deldetection(Detectionre detec);

    @Update("UPDATE detection_report set score=#{score},type =#{type},level =#{level},content =#{content},creator =#{creator},delete_flag=#{delete_flag},update_date =#{update_date} "+
            "where id=#{id}")
    int updetection(Detectionre detec);

    @Select(" SELECT * FROM detection_report limit #{page},#{pageSize} ")
    List<DetectionreEntity> selectDetectionreList(@Param("page") Integer page, @Param("pageSize") Integer pageSize);

    @Select(" SELECT count(*) FROM detection_report")
    int selectDetectionreListCount();


    @Select("select * from `detection_report` where creator = #{creator}")
    List<Detectionre> selectDetectionreList(Detectionre detec);












}
