package sinfo.common.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import sinfo.model.entity.SysUserEntity;
import sinfo.model.model.SysUserModel;

import java.util.List;

@Mapper
public interface SysUserDao extends BaseMapper<SysUserEntity> {


    @Select({" <script>",
            " SELECT s.*,r.`name` as rolename FROM sys_user s LEFT JOIN roles r on r.id=s.role_id WHERE s.delete_flag = 0 ",
            " <if test='keyword != null'> ",
            " AND (s.account like concat('%',#{keyword,jdbcType=VARCHAR},'%') or s.name like concat('%',#{keyword,jdbcType=VARCHAR},'%')) ",
            " </if>",
            " AND r.delete_flag=0 ORDER BY s.create_date desc limit #{page},#{pageSize} ",
            " </script>"})
    List<SysUserModel> selectSysUserList(@Param("keyword") String keyword, @Param("page") Integer page, @Param("pageSize") Integer pageSize);


    @Select({" <script>",
            " SELECT count(*) FROM sys_user s LEFT JOIN roles r on r.id=s.role_id WHERE s.delete_flag = 0 ",
            " <if test='keyword != null'> ",
            " AND (s.account like concat('%',#{keyword,jdbcType=VARCHAR},'%') or s.name like concat('%',#{keyword,jdbcType=VARCHAR},'%')) ",
            " </if>",
            " AND r.delete_flag=0 ",
            " </script>"})
    int selectSysUserListCount(@Param("keyword") String keyword);

}
