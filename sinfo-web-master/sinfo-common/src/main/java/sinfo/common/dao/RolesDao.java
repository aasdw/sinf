package sinfo.common.dao;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;
import sinfo.common.Classinstance.Detectionre;
import sinfo.common.Classinstance.Roles;
import sinfo.model.entity.RolesEntity;


import java.util.List;

@Mapper
public interface RolesDao extends BaseMapper<RolesEntity>{


    @Insert(" INSERT INTO sif_roles (role_name, role_remark,role_status,role_permissions,delete_flag,create_date,update_date) " +
            " VALUES (#{role_name},#{role_remark},#{role_status},#{role_permissions},#{delete_flag},#{create_date},#{update_date})")
    int addroles(Roles roles);


    @Update("UPDATE sif_roles set role_name=#{role_name},role_remark =#{role_remark},role_status =#{role_status},role_permissions =#{role_permissions},delete_flag =#{delete_flag},update_date=#{update_date} "+
            "where id=#{id}")
    int updateroles(Roles roles);


    @Delete("delete from sif_roles   where id =#{id}")
    int delroles(Roles roles);


    @Select(" SELECT * FROM sif_roles limit #{page},#{pageSize} ")
    List<RolesEntity> selectRoleList(@Param("page") Integer page, @Param("pageSize") Integer pageSize);

    @Select(" SELECT count(*) FROM sif_roles")
    int selectRoleListCount();



}









