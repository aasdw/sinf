package sinfo.common.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import sinfo.model.entity.WechatDeviceLoginTimeEntity;

@Mapper
public interface WechatDeviceLoginTimeDao extends BaseMapper<WechatDeviceLoginTimeEntity> {
}
