package sinfo.common.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import sinfo.model.entity.UserEntity;

import java.util.List;

@Mapper
public interface UserDao extends BaseMapper<UserEntity> {
    /*@Results(id = "wechatInfoMap", value = {
            @Result(property = "wechatInfoId", column = "wechat_info_id"),
            @Result(property = "wxId", column = "wx_id"),
            @Result(property = "deviceId", column = "device_id"),
            @Result(property = "nickName", column = "nick_name"),
            @Result(property = "onlineStatus", column = "online_status"),
            @Result(property = "loginDate", column = "login_date"),
            @Result(property = "offlineDate", column = "offline_date"),
            @Result(property = "lastHeartBeatDate", column = "last_heart_beat_date")
    })
    @Select("SELECT * FROM gongmall_contract WHERE openid = #{openid} AND delete_flag = 0")
    UserModel getBean(@Param("openid") String deviceId);

    @Insert(" INSERT INTO gongmall_contract (openid, name, mobile, id_number, bank_num, bank_name, work_number,reserve_mobile ) " +
            " VALUES (#{openid},#{name},#{mobile},#{idNumber},#{bankNum},#{bankName},#{openid},#{mobile})")
    int addUserInfo(UserBo userBo);*/

    @Select(" SELECT * FROM member WHERE  delete_flag = 0 order by create_date desc limit #{page},#{pageSize} ")
    List<UserEntity> selectMemberList(@Param("page") Integer page, @Param("pageSize") Integer pageSize);

    @Select(" SELECT count(*) FROM member WHERE  delete_flag = 0 ")
    int selectMemberListCount();

}
