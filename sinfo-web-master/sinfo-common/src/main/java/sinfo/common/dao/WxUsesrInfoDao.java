package sinfo.common.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import sinfo.model.entity.WxUserInfoEntity;

import java.util.List;

@Mapper
public interface WxUsesrInfoDao extends BaseMapper<WxUserInfoEntity> {

    @Select({"<script>",
            " SELECT * FROM wx_user_info WHERE delete_flag=0 ",
            "<if test='keyword != null'> ",
            " AND (phone like concat('%',#{keyword,jdbcType=VARCHAR},'%') or nick_name like concat('%',#{keyword,jdbcType=VARCHAR},'%')) ",
            "</if>",
            " ORDER BY create_date desc limit #{page},#{pageSize}",
            "</script>"})
    List<WxUserInfoEntity> selectCustomerList(@Param("keyword") String keyword, @Param("page") Integer page, @Param("pageSize") Integer pageSize);

    @Select({"<script>",
            " SELECT count(*) FROM wx_user_info WHERE delete_flag=0 ",
            "<if test='keyword != null'> ",
            " AND (phone like concat('%',#{keyword,jdbcType=VARCHAR},'%') or nick_name like concat('%',#{keyword,jdbcType=VARCHAR},'%')) ",
            "</if>",
            "</script>"})
    int selectCustomerListCount(@Param("keyword") String keyword);

}
