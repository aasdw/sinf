package sinfo.model.entity;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;


import java.util.Date;

@TableName("sif_roles")
public class RolesEntity {

    @JsonIgnore
    @TableId("id")
    private Integer  id;
    private String   role_name;
    private String   role_remark;
    private Integer  role_status;
    private String   role_permissions;
    private Integer  delete_flag;
    private Date     create_date;
    private Date     update_date;

    public void setId(Integer id){
        this.id=id;
    }

    public Integer getId(){

        return id;
    }


    public void setRole_name(String role_name){
        this.role_name=role_name;
    }

    public String getRole_name(){

        return role_name;
    }



    public void setRole_remark(String role_remark){
        this.role_remark=role_remark;
    }

    public String getRole_remark(){

        return role_remark;
    }


    public void setRole_status(Integer role_status){
        this.role_status=role_status;
    }

    public Integer getRole_status(){

        return role_status;
    }


    public void setRole_permissions(String role_permissions){
        this.role_permissions=role_permissions;
    }



    public String getRole_permissions(){

        return role_permissions;
    }




    public void setDelete_flag(Integer delete_flag){
        this.delete_flag=delete_flag;
    }

    public Integer getDelete_flag(){

        return delete_flag;
    }


    public void setCreate_date(Date create_date){
        this.create_date=create_date;
    }

    public Date getCreate_date(){

        return create_date;
    }



    public void setUpdate_date(Date update_date){
        this.update_date=update_date;
    }

    public Date getUpdate_date(){

        return update_date;
    }








}
