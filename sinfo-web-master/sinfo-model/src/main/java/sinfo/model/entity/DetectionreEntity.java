package sinfo.model.entity;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;

@TableName("detection_report")
public class DetectionreEntity {

@JsonIgnore
@TableId("id")
private  Integer  id;
private  String  score;
private  Integer type;
private  String  level;
private  String  content;
private  String  creator;
private  String  delete_flag;
private  String  create_date;
private  String  update_date;




    public void setId(Integer id){

        this.id=id;

    }

    public Integer  getId(){

        return id;

    }



    public void setScore(String score){

        this.score=score;

    }

    public String  getScore(){


        return score;

    }


    public void setType(Integer type){


        this.type=type;

    }

    public Integer  getType(){


        return type;

    }



    public void setLevel(String  level){


        this.level=level;

    }

    public String   getLevel(){


        return level;

    }




    public void  setContent(String  content){


        this.content=content;

    }

    public String   getContent(){


        return content;

    }



    public void  setCreator(String  creator){


        this.creator=creator;

    }

    public String   getCreator(){


        return creator;

    }


    public void  setDelete_flag(String  delete_flag){


        this.delete_flag=delete_flag;

    }

    public String   getDelete_flag(){


        return delete_flag;

    }




    public void  setCreate_date(String  create_date){


        this.create_date=create_date;

    }


    public String   getCreate_date(){


        return create_date;

    }



    public void  setUpdate_date(String  update_date){


        this.update_date=update_date;

    }


    public String   getUpdate_date(){


        return update_date;

    }





}
