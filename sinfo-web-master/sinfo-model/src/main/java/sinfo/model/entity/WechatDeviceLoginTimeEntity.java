package sinfo.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.time.LocalDateTime;

@TableName("wechat_device_login_time")
public class WechatDeviceLoginTimeEntity {
    /**
     * 主键
     */
    @TableId(value = "login_id",type = IdType.AUTO)
    private Integer loginId;
    /**
     * 微信关联ID
     */
    private String wechatInfoId;
    /**
     * 设备ID
     */
    private String deviceId;
    /**
     * 登录时长(小时)
     */
    private Integer loginTime;
    /**
     * 创建时间
     */
    private LocalDateTime createDate;
    /**
     * 更新时间
     */
    private LocalDateTime updateDate;

    public Integer getLoginId() {
        return loginId;
    }

    public void setLoginId(Integer loginId) {
        this.loginId = loginId;
    }

    public String getWechatInfoId() {
        return wechatInfoId;
    }

    public void setWechatInfoId(String wechatInfoId) {
        this.wechatInfoId = wechatInfoId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Integer loginTime) {
        this.loginTime = loginTime;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public LocalDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
    }
}
