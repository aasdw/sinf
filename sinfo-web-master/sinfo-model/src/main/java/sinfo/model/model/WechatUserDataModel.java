package sinfo.model.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class WechatUserDataModel {
    /**
     * 微信Id或者群Id
     */
    private String wxId;
    /**
     * 微信昵称
     */
    private String nickName;
    /**
     * 微信号
     */
    private String aliasName;
    /**
     * 微信头像
     */
    private String headImageUrl;
    @JsonIgnore
    private String serverInfo;

    public String getWxId() {
        return wxId;
    }

    public void setWxId(String wxId) {
        this.wxId = wxId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getHeadImageUrl() {
        return headImageUrl;
    }

    public void setHeadImageUrl(String headImageUrl) {
        this.headImageUrl = headImageUrl;
    }

    public String getServerInfo() {
        return serverInfo;
    }

    public void setServerInfo(String serverInfo) {
        this.serverInfo = serverInfo;
    }
}
