package sinfo.model.enums;

public enum WechatServerType {
    WX_SERVER_SHEQUN("SHEQUN","社群微信api");

    private String serverName;
    private String info;

    WechatServerType(String serverName, String info) {
        this.serverName = serverName;
        this.info = info;
    }

    public String getServerName() {
        return serverName;
    }

    public String getInfo() {
        return info;
    }

}
