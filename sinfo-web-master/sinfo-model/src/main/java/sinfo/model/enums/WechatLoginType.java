package sinfo.model.enums;

public enum WechatLoginType {
    Unknown(-1, "未知"),
    QrCodeLogin(0, "扫码登录"),
    AwakenLogin(1, "唤醒登录");
    private Integer code;
    private String info;

    WechatLoginType(Integer code, String info) {
        this.code = code;
        this.info = info;
    }

    public Integer getCode() {
        return code;
    }

    public String getInfo() {
        return info;
    }
    public static WechatLoginType valueOf(Integer code) {
        for (WechatLoginType type : WechatLoginType.values()) {
            if (type.getCode().equals(code)) {
                return type;
            }
        }
        return Unknown;
    }
}
