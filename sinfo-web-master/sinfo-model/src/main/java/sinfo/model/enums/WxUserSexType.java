package sinfo.model.enums;

public enum WxUserSexType {
    Unknown(-1, "未知"),
    Female(0, "女"),
    Man(1, "男");
    private Integer code;
    private String name;

    WxUserSexType(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static WxUserSexType valueOf(Integer code) {
        for (WxUserSexType type : WxUserSexType.values()) {
            if (type.getCode().equals(code)) {
                return type;
            }
        }
        return Unknown;
    }
}
