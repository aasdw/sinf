package sinfo.model.constant;

public class HttpConstants {
    public static final String CANT_FIND_SERVER = "服务不存在！";
    public static final Integer SUCCESS = 0;
    public static final Integer ERROR = 1;
    public static final Integer OTHER_ERROR = 2;
    public static final Integer SHEQUN_SUCCESS = 1000;
    public static final Integer SHEQUN_ERROR = 1001;
    //get方法
    public static final String GetMethod = "GET";
    //post方法
    public static final String PostMethod = "POST";
    public static final Integer FORBID_REQUEST = 403;

    public static final String FORBID_REQUEST_MSG = "error request";
}
