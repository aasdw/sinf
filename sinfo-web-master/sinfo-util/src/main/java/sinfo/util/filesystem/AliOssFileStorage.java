package sinfo.util.filesystem;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.GenericResult;
import com.aliyun.oss.model.ProcessObjectRequest;
import com.aliyun.oss.model.PutObjectResult;
import sinfo.util.util.FileUtil;
import org.apache.commons.lang3.StringUtils;

import java.io.InputStream;

public class AliOssFileStorage implements IFileStorage {

    /**
     * 默认连接超时 50 秒，操作超时 300 秒
     *
     * @see com.aliyun.oss.ClientConfiguration
     */
    private final OSS ossClient;
    private final String bucket;

    public AliOssFileStorage(String endpoint, String accessKeyId, String accessKeySecret, String bucket) {
        ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        this.bucket = bucket;
    }

    @Override
    public String store(InputStream inputStream, String suffix) throws Exception {
        return store("", inputStream, suffix);
    }

    @Override
    public String store(String folder, InputStream inputStream, String suffix) throws Exception {
        if (!StringUtils.isEmpty(folder)) {
            if (!StringUtils.isNotBlank(folder)) {
                throw new IllegalArgumentException("Folder Name can not contains Blank String.");
            }
        }
        if (!ossClient.doesBucketExist(this.bucket)) {
            throw new Exception("Bucket Not Exists.");
        }
        String filePath = FileUtil.generateFilePath(suffix);
        PutObjectResult putObjectResult = ossClient.putObject(this.bucket, filePath, inputStream);
        if (null == putObjectResult) {
            throw new Exception("Upload File Failed.");
        }
        return filePath;
    }

    @Override
    public void delete(String filePath) throws Exception {
        if (!ossClient.doesBucketExist(this.bucket)) {
            throw new Exception("Bucket Not Exists.");
        }

        ossClient.deleteObject(this.bucket, filePath);
    }

    @Override
    public void destroy() {
        ossClient.shutdown();
    }

    @Override
    public void process(String sourceImage, String queries) throws Exception {
        ProcessObjectRequest request = new ProcessObjectRequest(this.bucket, sourceImage, queries);
        GenericResult result = ossClient.processObject(request);
        if (!result.getResponse().isSuccessful()) {
            throw new Exception(result.getResponse().getErrorResponseAsString());
        }
    }


}
