package sinfo.util.filesystem;

import java.io.InputStream;

public interface IFileStorage {

    String store(InputStream inputStream, String suffix) throws Exception;

    String store(String folder, InputStream inputStream, String suffix) throws Exception;

    void delete(String filePath) throws Exception;

    void destroy();

    void process(String sourceImage, String queries) throws Exception;
}
