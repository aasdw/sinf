package sinfo.util.util;

import org.apache.http.Header;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.config.SocketConfig;
import org.apache.http.conn.DnsResolver;
import org.apache.http.conn.HttpConnectionFactory;
import org.apache.http.conn.ManagedHttpClientConnection;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.DefaultConnectionReuseStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultConnectionKeepAliveStrategy;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.DefaultHttpResponseParserFactory;
import org.apache.http.impl.conn.ManagedHttpClientConnectionFactory;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.impl.conn.SystemDefaultDnsResolver;
import org.apache.http.impl.io.DefaultHttpRequestWriterFactory;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class HttpClientUtils {
    private static PoolingHttpClientConnectionManager manager = null;
    private static CloseableHttpClient httpClient = null;
    private final static Logger logger = LoggerFactory.getLogger(HttpClientUtils.class);

    public static synchronized CloseableHttpClient getHttpClient() {
        if (httpClient == null) {
            //注册访问协议相关的Socket工厂
            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder
                    .<ConnectionSocketFactory>create()
                    .register("http", PlainConnectionSocketFactory.INSTANCE)
                    .register("https", SSLConnectionSocketFactory.getSystemSocketFactory())
                    .build();

            //HttpConnection 工厂:配置写请求/解析响应处理器
            HttpConnectionFactory<HttpRoute, ManagedHttpClientConnection> connectionFactory
                    = new ManagedHttpClientConnectionFactory(
                    DefaultHttpRequestWriterFactory.INSTANCE,
                    DefaultHttpResponseParserFactory.INSTANCE);
            //DNS 解析器
            DnsResolver dnsResolver = SystemDefaultDnsResolver.INSTANCE;
            //创建池化连接管理器
            manager = new PoolingHttpClientConnectionManager(socketFactoryRegistry, connectionFactory, dnsResolver);
            //默认为Socket配置
            SocketConfig defaultSocketConfig = SocketConfig.custom().setTcpNoDelay(true).build();
            manager.setDefaultSocketConfig(defaultSocketConfig);
            //设置整个连接池的最大连接数
            manager.setMaxTotal(200);
            //每个路由的默认最大连接，每个路由实际最大连接数由DefaultMaxPerRoute控制，而MaxTotal是整个池子的最大数
            //设置过小无法支持大并发(ConnectionPoolTimeoutException) Timeout waiting for connection from pool
            //每个路由的最大连接数 每个独立的host为1个路由
            manager.setDefaultMaxPerRoute(150);
            //在从连接池获取连接时，连接不活跃多长时间后需要进行一次验证，默认为5s
            manager.setValidateAfterInactivity(5 * 1000);
            //默认请求配置
            RequestConfig defaultRequestConfig = RequestConfig.custom()
                    //设置连接超时时间，60s
                    .setConnectTimeout(60 * 1000)
                    //设置等待数据超时时间，60s
                    .setSocketTimeout(60 * 1000)
                    //设置从连接池获取连接的等待超时时间
                    .setConnectionRequestTimeout(2000)
                    .build();
            //创建HttpClient
            httpClient = HttpClients.custom()
                    .setConnectionManager(manager)
                    //连接池不是共享模式
                    .setConnectionManagerShared(false)
                    //定期回收空闲连接
                    .evictIdleConnections(60, TimeUnit.SECONDS)
                    // 定期回收过期连接
                    .evictExpiredConnections()
                    //连接存活时间，如果不设置，则根据长连接信息决定
                    .setConnectionTimeToLive(60, TimeUnit.SECONDS)
                    //设置默认请求配置
                    .setDefaultRequestConfig(defaultRequestConfig)
                    //连接重用策略，即是否能keepAlive
                    .setConnectionReuseStrategy(DefaultConnectionReuseStrategy.INSTANCE)
                    //长连接配置，即获取长连接生产多长时间
                    .setKeepAliveStrategy(DefaultConnectionKeepAliveStrategy.INSTANCE)
                    //设置重试次数，默认是3次
                    .setRetryHandler(new DefaultHttpRequestRetryHandler())
                    .build();

            //添加钩子方法 当JVM 停止或重启时，关闭连接池释放掉连接
            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    try {
                        if (httpClient != null) {
                            httpClient.close();
                        }
                    } catch (IOException e) {
                        logger.error("error when close httpClient:{}", e);
                    }
                }
            });
        }
        return httpClient;
    }

    /**
     * post请求传输map数据
     *
     * @param url 请求地址
     * @param map 传递的参数
     * @return
     */
    public static String sendPostDataByMap(String url, Map<String, String> map) {
        String result = "";
        CloseableHttpResponse response = null;
        // 创建post方式请求对象
        HttpPost httpPost = new HttpPost(url);
        try {
            // 创建httpclient对象
            CloseableHttpClient httpClient = getHttpClient();
            // 装填参数
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            if (map != null) {
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    nameValuePairs.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
                }
            }
            // 设置参数到请求对象中
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "utf-8"));
            // 设置header信息
            // 指定报文头【Content-type】、【User-Agent】
            httpPost.setHeader("Content-type", "application/x-www-form-urlencoded");
            httpPost.setHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");
            // 执行请求操作，并拿到结果（同步阻塞）
            response = httpClient.execute(httpPost);
            // 获取结果实体
            // 判断网络连接状态码是否正常(0--200都数正常)
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                result = EntityUtils.toString(response.getEntity(), "utf-8");
            } else {
                httpPost.abort();
            }
            // 释放链接
            response.close();
            return result;
        } catch (Exception e) {
            logger.error("url-->" + url + " map-->" + map + e.getMessage(), e);
            // 释放链接
            try {
                httpPost.abort();
                if (response != null) {
                    response.close();
                }
            } catch (IOException ex) {
                logger.error("url-->" + url + " map-->" + map + ex.getMessage(), ex);
            }
            return result;
        }
    }

    /**
     * post请求传输json数据
     *
     * @param url  请求地址
     * @param json 传递的参数
     * @return
     */
    public static String sendPostDataByJson(String url, String json, Header[] headers) {
        return sendPostDataByJson(url, json, headers, null);
    }

    /**
     * post请求传输json数据
     *
     * @param url  请求地址
     * @param json 传递的参数
     * @return
     */
    public static String sendPostDataByJson(String url, String json, Header[] headers, Integer timeOut) {
        String result = "";
        CloseableHttpResponse response = null;
        // 创建post方式请求对象
        HttpPost httpPost = new HttpPost(url);
        if (Objects.nonNull(headers)) {
            httpPost.setHeaders(headers);
        }
        if (Objects.nonNull(timeOut)) {
            RequestConfig requestConfig = RequestConfig.custom()
                    //设置连接超时时间
                    .setConnectTimeout(timeOut)
                    //设置等待数据超时时间
                    .setSocketTimeout(timeOut)
                    //设置从连接池获取连接的等待超时时间
                    .build();
            httpPost.setConfig(requestConfig);
        }
        try {
            // 创建httpclient对象
            CloseableHttpClient httpClient = HttpClientUtils.getHttpClient();
            // 设置参数到请求对象中
            StringEntity stringEntity = new StringEntity(json, ContentType.APPLICATION_JSON);
            // "utf-8"
            stringEntity.setContentEncoding("utf-8");
            httpPost.setEntity(stringEntity);
            // 执行请求操作，并拿到结果（同步阻塞）
            response = httpClient.execute(httpPost);
            // 获取结果实体
            // 判断网络连接状态码是否正常(0--200都数正常)
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                result = EntityUtils.toString(response.getEntity(), "utf-8");
            } else {
                httpPost.abort();
            }
            // 释放链接
            response.close();
            return result;
        } catch (Exception e) {
            logger.error("url-->" + url + " params-->" + json + e.getMessage(), e);
            // 释放链接
            try {
                httpPost.abort();
                if (response != null) {
                    response.close();
                }
            } catch (IOException ex) {
                logger.error("url-->" + url + " params-->" + json + ex.getMessage(), ex);
            }
            return result;
        }
    }

    /**
     * post请求传输json数据
     *
     * @param url 请求地址
     * @return
     */
    public static String sendPostDataByUrlEncoded(String url, List<NameValuePair> params, Header[] headers, int timeOut) {
        String result = "";
        CloseableHttpResponse response = null;
        // 创建post方式请求对象
        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeaders(headers);
        RequestConfig requestConfig = RequestConfig.custom()
                //设置连接超时时间
                .setConnectTimeout(timeOut)
                //设置等待数据超时时间
                .setSocketTimeout(timeOut)
                //设置从连接池获取连接的等待超时时间
                .setConnectionRequestTimeout(10000)
                .build();
        httpPost.setConfig(requestConfig);
        try {
            // 创建httpclient对象
            CloseableHttpClient httpClient = HttpClientUtils.getHttpClient();
            // "utf-8"
            httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            httpPost.setHeader("Content-type", "application/x-www-form-urlencoded");
            // 执行请求操作，并拿到结果（同步阻塞）
            response = httpClient.execute(httpPost);
            // 获取结果实体
            // 判断网络连接状态码是否正常(0--200都数正常)
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                result = EntityUtils.toString(response.getEntity(), "utf-8");
            } else {
                httpPost.abort();
            }
            // 释放链接
            response.close();
            return result;
        } catch (Exception e) {
            logger.error("url-->" + url + " params-->" + JacksonUtil.toJson(params) + e.getMessage(), e);
            // 释放链接
            try {
                httpPost.abort();
                if (response != null) {
                    response.close();
                }
            } catch (IOException ex) {
                logger.error("url-->" + url + " params-->" + JacksonUtil.toJson(params) + ex.getMessage(), ex);
            }
            return result;
        }
    }

    /**
     * get请求传输数据
     *
     * @param url
     * @return
     */
    public static String sendGetData(String url, Header[] headers) {
        String result = "";
        CloseableHttpResponse response = null;
        // 创建get方式请求对象
        HttpGet httpGet = new HttpGet(url);
        httpGet.setHeaders(headers);
        try {
            // 创建httpclient对象
            CloseableHttpClient httpClient = HttpClientUtils.getHttpClient();

            httpGet.addHeader("Content-type", "application/json");
            // 通过请求对象获取响应对象
            response = httpClient.execute(httpGet);

            // 获取结果实体
            // 判断网络连接状态码是否正常(0--200都数正常)
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                result = EntityUtils.toString(response.getEntity(), "utf-8");
            } else {
                httpGet.abort();
            }
            // 释放链接
            response.close();
            return result;
        } catch (Exception e) {
            logger.error("url-->" + url + e.getMessage(), e);
            try {
                httpGet.abort();
                if (response != null) {
                    response.close();
                }
            } catch (IOException ex) {
                logger.error("url-->" + url + ex.getMessage(), ex);
            }
            return result;
        }
    }
}
