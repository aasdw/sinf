package sinfo.util.util;


import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class DbUtil {

    public static final List<String> MODULE_LIST = Arrays.asList(
            "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "0a", "0b", "0c", "0d", "0e", "0f",
            "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "1a", "1b", "1c", "1d", "1e", "1f",
            "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "2a", "2b", "2c", "2d", "2e", "2f",
            "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "3a", "3b", "3c", "3d", "3e", "3f",
            "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "4a", "4b", "4c", "4d", "4e", "4f",
            "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "5a", "5b", "5c", "5d", "5e", "5f",
            "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "6a", "6b", "6c", "6d", "6e", "6f",
            "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "7a", "7b", "7c", "7d", "7e", "7f",
            "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "8a", "8b", "8c", "8d", "8e", "8f",
            "90", "91", "92", "93", "94", "95", "96", "97", "98", "99", "9a", "9b", "9c", "9d", "9e", "9f",
            "a0", "a1", "a2", "a3", "a4", "a5", "a6", "a7", "a8", "a9", "aa", "ab", "ac", "ad", "ae", "af",
            "b0", "b1", "b2", "b3", "b4", "b5", "b6", "b7", "b8", "b9", "ba", "bb", "bc", "bd", "be", "bf",
            "c0", "c1", "c2", "c3", "c4", "c5", "c6", "c7", "c8", "c9", "ca", "cb", "cc", "cd", "ce", "cf",
            "d0", "d1", "d2", "d3", "d4", "d5", "d6", "d7", "d8", "d9", "da", "db", "dc", "dd", "de", "df",
            "e0", "e1", "e2", "e3", "e4", "e5", "e6", "e7", "e8", "e9", "ea", "eb", "ec", "ed", "ee", "ef",
            "f0", "f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9", "fa", "fb", "fc", "fd", "fe", "ff");

    public static final int DB_COUNT = 4;

    public static final int TABLE_COUNT = 8;

    public static final String BASE_MODULE = "00";

    public static String wrapKeyword(String keyword) {
        if (StringUtils.isEmpty(keyword)) {
            return "";
        }
        return "%" + keyword + "%";
    }

    public static int getModuleIndex(String memberId) {
        if (memberId.length() < 2) {
            throw new IllegalArgumentException("");
        }
        String module = memberId.substring(memberId.length() - 2);
        return MODULE_LIST.indexOf(module);
    }

    private static int getModule(String memberId) {
        return getModuleIndex(memberId) % (DB_COUNT * TABLE_COUNT);
    }

    public static String getDbModule(String memberId) {
        int index = getModule(memberId) / TABLE_COUNT;
        return String.format("%02d", index);
    }

    public static String getTableModule(String memberId) {
        int index = getModule(memberId) % TABLE_COUNT;
        return String.format("%02d", index);
    }

    public static String getDbModuleByIndex(int index) {
        return MODULE_LIST.get(index * TABLE_COUNT);
    }

    public static String getRandomDbModule() {
        long currentSecond = System.currentTimeMillis();
        return getDbModuleByIndex((int) (currentSecond % DB_COUNT));
    }

    public static String generateOrderCode(String memberId) {
        int moduleIndex = getModuleIndex(memberId);
        // TODO 随机数据替换为 redis 自增
        return String.format("%d%03d%03d", System.currentTimeMillis(), moduleIndex, new Random().nextInt(1000));
    }

    public static String getModuleByCode(String code) {
        if (code.length() < 3) {
            throw new IllegalArgumentException();
        }
        String indexStr = code.substring(code.length() - 3);
        try {
            int moduleIndex = Integer.parseInt(indexStr);
            return MODULE_LIST.get(moduleIndex);
        } catch (Exception ex) {
            throw new IllegalArgumentException();
        }
    }

    public static void main(String[] args) {
        System.out.println(DbUtil.getTableModule("d12f5030fe6b45c2bb313b55125c197b"));
    }
}
