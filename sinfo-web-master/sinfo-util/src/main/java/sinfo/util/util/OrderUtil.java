package sinfo.util.util;

/**
 * Created with IntelliJ IDEA.
 * Description: 扩展工具订单类
 * User: xiaok
 * Email: 306934150@qq.com
 * Date: 2018-09-04
 * Time: 15:23
 */
public class OrderUtil {
    protected static final IdWorker idWorker = new IdWorker(1);

    public static String generate() {
        try {
            long id = idWorker.nextId();
            return String.valueOf(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
