package sinfo.util.util;


import sinfo.util.cache.ICacheManager;

public class CacheManagerUtil {

    private static ICacheManager cacheManager;

    private CacheManagerUtil() {

    }

    public static void setCacheManager(ICacheManager manager) {
        cacheManager = manager;
    }

    public static ICacheManager getCacheManager() {
        synchronized (CacheManagerUtil.class) {
            if (null != cacheManager) {
                return cacheManager;
            }
            throw new RuntimeException("No Cache Manage Set.");
        }
    }
}
