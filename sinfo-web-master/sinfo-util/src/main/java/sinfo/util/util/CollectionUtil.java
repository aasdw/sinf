package sinfo.util.util;

import java.util.Collection;

public class CollectionUtil {
    public final static boolean isEmpty(Collection collection) {
        return collection == null ? true : collection.isEmpty();
    }

    public final static boolean nonEmpty(Collection collection) {
        return !isEmpty(collection);
    }
}
