package sinfo.util.util;

import com.google.common.base.Strings;
import org.apache.commons.lang3.StringUtils;

public class DesensitizedUtils {

    /**
     * 脱敏规则: 只显示第一个汉字,比如李某某置换为李**, 李某置换为李*
     * @param fullName
     * @return
     */
    public static String desensitizedName(String fullName){
        if (!Strings.isNullOrEmpty(fullName)) {
            String name = StringUtils.left(fullName, 1);
            return StringUtils.rightPad(name, StringUtils.length(fullName), "*");
        }
        return fullName;
    }

    /**
     * 脱敏规则: 保留前三后四, 比如15638296218置换为156****6218
     * @param phoneNumber
     * @return
     */
    public static String desensitizedPhoneNumber(String phoneNumber){
        if(StringUtils.isNotEmpty(phoneNumber)){
            phoneNumber = phoneNumber.replaceAll("(\\w{3})\\w*(\\w{4})", "$1****$2");
        }
        return phoneNumber;
    }

    /**
     * 脱敏规则: 保留前六后三, 适用于15位和18位身份证号
     * @param idNumber
     * @return
     */
    public static String desensitizedIdNumber(String idNumber){
        if (!Strings.isNullOrEmpty(idNumber)) {
            if (idNumber.length() == 15){
                idNumber = idNumber.replaceAll("(\\w{6})\\w*(\\w{3})", "$1******$2");
            }
            if (idNumber.length() == 18){
                idNumber = idNumber.replaceAll("(\\w{6})\\w*(\\w{3})", "$1*********$2");
            }
        }
        return idNumber;
    }

    /*public static String desensitizedIdNumber1(String idNumber){
        if (!Strings.isNullOrEmpty(idNumber)) {
            return StringUtils.left(idNumber, 6).concat(StringUtils.removeStart(StringUtils.leftPad(StringUtils.right(idNumber, 3), StringUtils.length(idNumber), "*"), "******"));
        }
        return idNumber;
    }*/

    /**
     * 脱敏规则: 从第4位开始隐藏,隐藏8位
     *
     * 因地址位数是不确定的,所以结尾长度为总长度减去 前面保留长度和隐藏长度之和 address.length()-11
     * @param address
     * @return
     */
    public static String desensitizedAddress(String address){
        if (!Strings.isNullOrEmpty(address)) {
            return StringUtils.left(address, 3).concat(StringUtils.removeStart(StringUtils.leftPad(StringUtils.right(address, address.length()-11), StringUtils.length(address), "*"), "***"));
        }
        return address;
    }
}
