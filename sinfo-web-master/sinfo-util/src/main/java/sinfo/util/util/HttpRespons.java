package sinfo.util.util;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Vector;


public class HttpRespons {
	  String urlString;     
      
	    int defaultPort;     
	      
	    String file;     
	      
	    String host;     
	      
	    String path;     
	      
	    int port;     
	      
	    String protocol;     
	      
	    String query;     
	      
	    String ref;     
	      
	    String userInfo;     
	      
	    String contentEncoding;     
	      
	    String content;     
	      
	    String contentType;     
	      
	    int code;     
	      
	    String message;     
	      
	    String method;     
	      
	    int connectTimeout;     
	      
	    int readTimeout;     
	      
	    Vector<String> contentCollection;     
	      
	    public String getContent() {     
	        return content;     
	    }     
	      
	    public String getContentType() {     
	        return contentType;     
	    }     
	      
	    public int getCode() {     
	        return code;     
	    }     
	      
	    public String getMessage() {     
	        return message;     
	    }     
	      
	    public Vector<String> getContentCollection() {     
	        return contentCollection;     
	    }     
	      
	    public String getContentEncoding() {     
	        return contentEncoding;     
	    }     
	      
	    public String getMethod() {     
	        return method;     
	    }     
	      
	    public int getConnectTimeout() {     
	        return connectTimeout;     
	    }     
	      
	    public int getReadTimeout() {     
	        return readTimeout;     
	    }     
	      
	    public String getUrlString() {     
	        return urlString;     
	    }     
	      
	    public int getDefaultPort() {     
	        return defaultPort;     
	    }     
	      
	    public String getFile() {     
	        return file;     
	    }     
	      
	    public String getHost() {     
	        return host;     
	    }     
	      
	    public String getPath() {     
	        return path;     
	    }     
	      
	    public int getPort() {     
	        return port;     
	    }     
	      
	    public String getProtocol() {     
	        return protocol;     
	    }     
	      
	    public String getQuery() {     
	        return query;     
	    }     
	      
	    public String getRef() {     
	        return ref;     
	    }     
	      
	    public String getUserInfo() {     
	        return userInfo;     
	    } 
	    
	    

	    /**
	          * 从输入流中读取数据
	          * 
	          * @param inStream
	          * @return
	          * @throws Exception
	    */
	    public static byte[] readInputStream(InputStream inStream) throws Exception {
	    	         ByteArrayOutputStream outStream = new ByteArrayOutputStream();
	    	         byte[] buffer = new byte[1024];
	    	         int len = 0;
	    	         while ((len = inStream.read(buffer)) != -1) {
	    	            outStream.write(buffer, 0, len);
	    	         }
	    	         byte[] data = outStream.toByteArray();
	    	         outStream.close();
	    	         inStream.close();
	    	        return data;
	    	     }
}
