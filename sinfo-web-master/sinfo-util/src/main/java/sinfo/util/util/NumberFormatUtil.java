package sinfo.util.util;

import java.text.DecimalFormat;

public class NumberFormatUtil {
    private static final ThreadLocal<DecimalFormat> decimalFormatThreadLocal = new ThreadLocal<DecimalFormat>() {
        @Override
        protected DecimalFormat initialValue() {
            return new DecimalFormat("0.00");
        }
    };

    /**
     * 保留两位小数
     * @return
     */
    public static final String doubleToString(double value){
        return decimalFormatThreadLocal.get().format(value);
    }
}
