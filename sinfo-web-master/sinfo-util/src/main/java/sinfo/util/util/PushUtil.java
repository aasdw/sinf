package sinfo.util.util;

import org.apache.commons.lang3.StringUtils;

import java.net.URLEncoder;
import java.util.*;

public class PushUtil {
    public static String getToken(Map<String, String> params,String appSecret) throws Exception{
        StringBuffer content = new StringBuffer();
        List<String> keys = new ArrayList();
        for (String key : params.keySet()) {
            switch (key) {
                case "appId":
                    keys.add(key);
                    break;
                case "timestamp":
                    keys.add(key);
                    break;
                case "requestId":
                    keys.add(key);
                    break;
                case "userCode":
                    keys.add(key);
                    break;
            }
        }
        Collections.sort(keys);
        for (int i = 0; i < keys.size(); i++) {
            String key = keys.get(i);
            String value = params.get(key).toString();
            if (StringUtils.isNotBlank(key) && StringUtils.isNotBlank(value)) {
                content.append(key).append(value);
            } else {
                return "key:" + key + "的值非法";
            }

        }
        System.out.println("加密字符串:" + content);
        return MD5Util.md5(content.append(appSecret).toString()).toUpperCase();
    }

    public static void main(String[] args) throws Exception {

            HashMap<String, String> params = new HashMap<>();
            params.put("appId", "ZF107");
            params.put("timestamp", System.currentTimeMillis() + "");
            params.put("requestId", UUID.randomUUID().toString());
            params.put("userCode", "ZF107");
            params.put("token", getToken(params, "2021bsjZF107"));
            params.put("devName", "BSJ2021071401");
//        params.put("money", "100.01");
            params.put("bizType", "2");

            params.put("content", URLEncoder.encode("通过云端操作对应的设备完成播报", "utf-8"));

            String s = HttpUtil.doGet("https://ioe.car900.com/v1/openApi/dev/controlDevice.json", params);
       System.out.println(s);
        }



}
