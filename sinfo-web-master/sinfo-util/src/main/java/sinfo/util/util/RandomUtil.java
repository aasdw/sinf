package sinfo.util.util;

import java.util.Random;
import java.util.UUID;

public class RandomUtil {

    public static synchronized String getUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    public static final long dx = 30 * 386 * 12 * 30 * 24 * 3600 * 1000; // starting at 2000 year
    public static long lastUUID = System.currentTimeMillis() - dx;

    public static synchronized long random() {
        long uuid = System.currentTimeMillis() - dx;
        while (uuid == lastUUID)
            uuid = System.currentTimeMillis() - dx;
        lastUUID = uuid;
        return uuid;
    }

    public static int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }
}
