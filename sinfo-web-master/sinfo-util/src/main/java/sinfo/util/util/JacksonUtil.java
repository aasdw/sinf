package sinfo.util.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import sinfo.util.jackson.JacksonObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;

public class JacksonUtil {
    private final static Logger logger = LoggerFactory.getLogger(JacksonUtil.class);

    private static final ObjectMapper objectMapper = new JacksonObjectMapper();

    public static String toJson(Object object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            return "";
        }
    }

    /**
     * 将JSON转换成object
     */
    public static <T> T fromJson(String json, Class<T> classOfType) {
        if (StringUtils.isEmpty(json)) {
            return null;
        }
        try {
            return objectMapper.readValue(json, classOfType);
        } catch (Exception e) {
            logger.error("JSON 解析失败：{}", e.getMessage());
            return null;
        }
    }

    /**
     * 将JSON转换成object
     */
    public static <T> T fromJson(String json, TypeReference<T> javaType) {
        if (StringUtils.isEmpty(json)) {
            return null;
        }
        try {
            return objectMapper.readValue(json, javaType);
        } catch (Exception e) {
            logger.error("JSON 解析失败：{}", e.getMessage());
            return null;
        }
    }
    /**
     * 将JSON转换成object
     */
    public static <T> T fromJson(String json, JavaType javaType) {
        if (StringUtils.isEmpty(json)) {
            return null;
        }
        try {
            return objectMapper.readValue(json, javaType);
        } catch (Exception e) {
            logger.error("JSON 解析失败：{}", e.getMessage());
            return null;
        }
    }

    /**
     * 转换集合类型
     */
    public static <T> List<T> convertList(String jsonString, Class<T> clazz) {
        if (StringUtils.isEmpty(jsonString)) {
            return new ArrayList<>();
        }

        JavaType javaType = getCollectionType(ArrayList.class, clazz);
        try {
            return objectMapper.readValue(jsonString, javaType);
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }

    public static <T> Map<String, T> convertMap(String jsonString, Class<T> classOfType) {
        if (StringUtils.isEmpty(jsonString)) {
            return new HashMap<>();
        }
        JavaType javaType = getCollectionType(HashMap.class, String.class, classOfType);
        try {
            return objectMapper.readValue(jsonString, javaType);
        } catch (IOException e) {
            return new HashMap<>();
        }
    }

    /**
     * 获取泛型的Collection Type
     *
     * @param collectionClass 泛型的Collection
     * @param elementClasses  元素类
     * @return JavaType Java类型
     * @since 1.0
     */
    private static JavaType getCollectionType(Class<?> collectionClass, Class<?>... elementClasses) {
        return objectMapper.getTypeFactory().constructParametricType(collectionClass, elementClasses);
    }

    /**
     * 根据Method获取返回值的JavaType
     * @param method
     * @return
     */
    public static JavaType getMethodReturnJavaType(Method method) {
        Type type = method.getGenericReturnType();
        //判断是否带有泛型
        if (type instanceof ParameterizedType) {
            Type[] actualTypeArguments = ((ParameterizedType) type).getActualTypeArguments();
            //获取泛型类型
            Class rowClass = (Class) ((ParameterizedType) type).getRawType();
            JavaType[] javaTypes = new JavaType[actualTypeArguments.length];
            for (int i = 0; i < actualTypeArguments.length; i++) {
                //泛型也可能带有泛型，递归获取
                javaTypes[i] = TypeFactory.defaultInstance().constructType(actualTypeArguments[i]);
            }
            return TypeFactory.defaultInstance().constructParametricType(rowClass, javaTypes);
        } else {
            //简单类型直接用该类构建
            return TypeFactory.defaultInstance().constructType(type);
        }
    }
}
