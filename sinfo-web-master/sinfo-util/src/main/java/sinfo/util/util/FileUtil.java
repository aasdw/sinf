package sinfo.util.util;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.UUID;

public class FileUtil {

    public static String generateFilePath(String suffix) {
        String uuid = UUID.randomUUID().toString().replace("-", "");
        StringBuilder filePathBuilder = new StringBuilder();
        for (int i = 1; i <= 3; i++) {
            filePathBuilder.append(uuid, (i - 1) * 2, i * 2);
            filePathBuilder.append("/");
        }
        return filePathBuilder.append(uuid).append(".").append(suffix).toString();
    }

    public static String downLoadFileToLocal(String remoteFile, String fileNewName) {
        if (remoteFile.startsWith("http") || remoteFile.startsWith("hhtps")) {
            File file = null;
            try {
                String fileExtName = "." + remoteFile.substring(remoteFile.lastIndexOf(".") + 1);
                String path = System.getProperty("user.dir") + File.separator + fileNewName + fileExtName;
                file = new File(path);
                // 校验文件是否存在
                if (!file.exists()) {
                    file.getParentFile().mkdirs();
                } else {
                    return path;
                }
                // 统一资源
                URL url = new URL(remoteFile);
                // 连接类的父类，抽象类
                URLConnection urlConnection = url.openConnection();
                // http的连接类
                HttpURLConnection httpURLConnection = (HttpURLConnection) urlConnection;
                //设置超时
                httpURLConnection.setConnectTimeout(1000 * 5);
                //设置请求方式，默认是GET
                httpURLConnection.setRequestMethod("GET");
                // 设置字符编码
                httpURLConnection.setRequestProperty("Charset", "UTF-8");
                // 打开到此 URL引用的资源的通信链接（如果尚未建立这样的连接）。
                httpURLConnection.connect();
                // 文件大小
                int fileLength = httpURLConnection.getContentLength();
                // 建立链接从请求中获取数据
                URLConnection con = url.openConnection();
                BufferedInputStream bin = new BufferedInputStream(httpURLConnection.getInputStream());
                // 指定文件名称(有需求可以自定义)


                OutputStream out = new FileOutputStream(file);
                int size = 0;
                int len = 0;
                byte[] buf = new byte[2048];
                while ((size = bin.read(buf)) != -1) {
                    len += size;
                    out.write(buf, 0, size);
                }
                // 关闭资源
                bin.close();
                out.close();
                return path;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return remoteFile;
    }
}
