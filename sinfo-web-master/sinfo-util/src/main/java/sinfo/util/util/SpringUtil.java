package sinfo.util.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.env.Environment;

public final class SpringUtil implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    private static byte environ = 0;

    @Override
    public void setApplicationContext(ApplicationContext mApplicationContext) throws BeansException {
        applicationContext = mApplicationContext;
        Environment environment = mApplicationContext.getEnvironment();
        String[] activeProfiles = environment.getActiveProfiles();
        for (String profile : activeProfiles) {
            if ("dev".equals(profile)) {

            }else if("prev".equals(profile)){
                environ = 1;
            }else if("prod".equals(profile)){
                environ = 2;
            }
            break;
        }

    }

    public static Object getBean(String beanName) {
        return applicationContext.getBean(beanName);
    }

    public static <T> T getBean(Class<T> classOfType) throws BeansException {
        return applicationContext.getBean(classOfType);
    }

    public static boolean isProd(){
        return environ == 2;
    }

    public static void main(String[] args) {

    }
}
