package sinfo.util.util;

import java.math.BigDecimal;

public class DataTypeConversionUtil {
    private static final BigDecimal UNIT = new BigDecimal("100");

    /**
     * BigDecimal转换成Long
     */
    public static long bigDecimalConversionLong(BigDecimal bigDecimal) {
        return bigDecimal.multiply(UNIT).longValue();
    }

    /**
     * BigDecimal除以100
     */
    public static BigDecimal divide(BigDecimal bigDecimal) {
        return bigDecimal.divide(UNIT).setScale(2, BigDecimal.ROUND_UP);
    }

}
