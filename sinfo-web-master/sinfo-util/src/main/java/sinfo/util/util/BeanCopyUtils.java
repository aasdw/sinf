package sinfo.util.util;

import com.esotericsoftware.reflectasm.MethodAccess;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;

public class BeanCopyUtils {

    private static Map<Class<?>, MethodAccess> methodMap = new HashMap<>();
    private static Map<Class<?>, Map<String, Integer>> methodIndexOfGet = new HashMap<>();
    private static Map<Class<?>, Map<String, Integer>> methodIndexOfSet = new HashMap<>();
    private static Map<Class<?>, Map<String, String>> methodIndexOfType = new HashMap<>();

    /**
     * 列表赋值
     * @author OprCalf
     * @param ts 待被赋值的列表
     * @param es 待赋值的列表
     * @param obj 待被赋值的列表里面的对象
     * @return List<T>
     */
    public static <T, E> List<T> copyList(Class<T> obj, List<E> es) {
        final List<T> ts = new ArrayList<>();
        if (!Objects.isNull(obj)) {
            es.forEach(item -> {
                try {
                    ts.add(copyProperties(obj.newInstance(), item));
                }
                catch (final Exception e) {
                    System.out.println("赋值列表出现异常:" + e.getMessage());
                }

            });
        }
        return ts;

    }

    /**
     * 浅copy类属性,根据属性名匹配，而不是类型+属性匹配，当类型不同且原属性值为null时，不变动目标类此属性值
     * @param desc 接收复制参数的类
     * @param orgi 原始类
     */
    public static <T, E> T copyProperties(T desc, E orgi) {
        if (orgi != null) {
            final MethodAccess descMethodAccess = methodAccessFactory(desc);
            final MethodAccess orgiMethodAccess = methodAccessFactory(orgi);
            final Map<String, Integer> get = methodIndexOfGet.get(orgi.getClass());
            final Map<String, Integer> set = methodIndexOfSet.get(desc.getClass());
            final Map<String, String> oritypemap = methodIndexOfType.get(orgi.getClass());
            final Map<String, String> desctypemap = methodIndexOfType.get(desc.getClass());

            List<String> sameField = null;
            if (get.size() < set.size()) {
                sameField = new ArrayList<>(get.keySet());
                sameField.retainAll(set.keySet());
            } else {
                sameField = new ArrayList<>(set.keySet());
                sameField.retainAll(get.keySet());
            }
            for (final String field : sameField) {
                final Integer setIndex = set.get(field);
                final Integer getIndex = get.get(field);
                final String oritype = oritypemap.get(field);
                final String desctype = desctypemap.get(field);
                final Object value = orgiMethodAccess.invoke(orgi, getIndex);
                try {
                    if (!oritype.equalsIgnoreCase(desctype)) {
                        if (value == null) {
                            continue;
                        }
                        switch (desctype) {
                            case "java.lang.String":
                                descMethodAccess.invoke(desc, setIndex.intValue(), value.toString());
                                break;
                            case "java.lang.Integer":
                            case "int":
                                descMethodAccess.invoke(desc, setIndex.intValue(), Integer.valueOf(value.toString()));
                                break;
                            case "java.lang.Long":
                            case "long":
                                descMethodAccess.invoke(desc, setIndex.intValue(), Long.valueOf(value.toString()));
                                break;
                            case "java.lang.Float":
                            case "float":
                                descMethodAccess.invoke(desc, setIndex.intValue(), Long.valueOf(value.toString()));
                                break;
                            case "java.lang.Boolean":
                            case "boolean":
                                descMethodAccess.invoke(desc, setIndex.intValue(), Boolean.valueOf(value.toString()));
                                break;
                            case "java.lang.Double":
                            case "double":
                                descMethodAccess.invoke(desc, setIndex.intValue(), Double.valueOf(value.toString()));
                                break;
                            case "java.lang.Byte":
                            case "byte":
                                descMethodAccess.invoke(desc, setIndex.intValue(), Byte.valueOf(value.toString()));
                                break;
                            case "java.lang.Short":
                            case "short":
                                descMethodAccess.invoke(desc, setIndex.intValue(), Short.valueOf(value.toString()));
                                break;
                            default:
                                break;
                        }
                    } else {
                        descMethodAccess.invoke(desc, setIndex.intValue(), value);
                    }
                }
                catch (final Exception e) {
                    System.out.println(field + ":" + e.getMessage());
                    return null;
                }
            }
            return desc;
        } else {
            return null;
        }
    }

    /**
     * 缓存信息
     * @author OprCalf
     * @param obj
     * @return MethodAccess
     */
    private static MethodAccess methodAccessFactory(Object obj) {
        MethodAccess descMethodAccess = methodMap.get(obj.getClass());
        if (descMethodAccess == null) {
            synchronized (obj.getClass()) {
                descMethodAccess = methodMap.get(obj.getClass());
                if (descMethodAccess != null) {
                    return descMethodAccess;
                }
                final Class<?> c = obj.getClass();
                final MethodAccess methodAccess = MethodAccess.get(c);
                final Field[] fields = c.getDeclaredFields();
                final Map<String, Integer> indexofget = new HashMap<>();
                final Map<String, Integer> indexofset = new HashMap<>();
                final Map<String, String> indexoftype = new HashMap<>();
                for (final Field field : fields) {
                    // 私有非静态
                    if (Modifier.isPrivate(field.getModifiers()) && !Modifier.isStatic(field.getModifiers())) {
                        // 获取属性名称
                        final String fieldName = captureName(field.getName());
                        // 获取get方法的下标
                        final int getIndex = methodAccess.getIndex("get" + fieldName);
                        // 获取set方法的下标
                        final int setIndex = methodAccess.getIndex("set" + fieldName);
                        indexofget.put(fieldName, getIndex);
                        indexofset.put(fieldName, setIndex);
                        indexoftype.put(fieldName, field.getType().getName());
                    }
                }
                methodIndexOfGet.put(c, indexofget);
                methodIndexOfSet.put(c, indexofset);
                methodIndexOfType.put(c, indexoftype);
                methodMap.put(c, methodAccess);
                return methodAccess;
            }
        }
        return descMethodAccess;
    }

    /**
     * 大小写转换
     * @author OprCalf
     * @param name
     * @return String
     */
    private static String captureName(String name) {
        final char[] cs = name.toCharArray();
        cs[0] -= 32;
        return String.valueOf(cs);
    }
}

