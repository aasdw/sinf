package sinfo.util.util;
import sinfo.util.message.BaseResponse;
import sinfo.util.message.HttpResponseStatus;

public class ResponseUtil {

    private ResponseUtil() {
    }

    public static BaseResponse data(Object data) {
        return data(data, "success");
    }

    public static BaseResponse data(Object data, String message) {
        BaseResponse response = new BaseResponse();
        response.setData(data);
        response.setCode(HttpResponseStatus.SUCCESS);
        response.setSuccess(true);
        response.setMessage(message);
        return response;
    }

    public static BaseResponse success() {
        return success("success");
    }

    public static BaseResponse success(String message) {
        BaseResponse response = new BaseResponse();
        response.setCode(HttpResponseStatus.SUCCESS);
        response.setSuccess(true);
        response.setMessage(message);
        return response;
    }

    public static BaseResponse error(String message) {
        return error(message, HttpResponseStatus.ERROR);
    }

    public static BaseResponse error(String message, int code) {
        BaseResponse response = new BaseResponse();
        response.setCode(code);
        response.setSuccess(false);
        response.setMessage(message);
        return response;
    }
}
