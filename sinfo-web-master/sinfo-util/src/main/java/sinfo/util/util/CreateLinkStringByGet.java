package sinfo.util.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * 参数的拼接整理
 * @author Lori
 * @version 2018年6月04日
 */
public class CreateLinkStringByGet {
	public static String createLinkStringByGet(Map<String, String> params){
		// TODO Auto-generated method stub
		List<String> keys = new ArrayList<String>(params.keySet());
		Collections.sort(keys);
		String str1 ="";
		for(int i=0;i<keys.size();i++) {
			String key = keys.get(i);
			Object value = params.get(key);//(String) 强制类型转换
			if(value instanceof Integer) {
				value = (Integer)value;
			}
			if(i==keys.size()-1) {
				
				str1 = str1+value.toString();
			}else {
				
				str1 = str1+value;
			}
		}
		System.out.println("待签名字符串"+str1);
		return str1;
	}
}
