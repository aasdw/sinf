package sinfo.util.cache.redis;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class RedisClusterBuilder {

    private static final String LOCALHOST = "127.0.0.1";

    private RedisConfig redisConfig;

    public RedisClusterBuilder() {
    }

    public RedisClusterBuilder(RedisConfig redisConfig) {
        this.redisConfig = redisConfig;
    }

    public JedisCluster build() {
        if (null == this.redisConfig) {
            throw new RuntimeException("Redis Config is null.");
        }
        // 设置 localhost 的为 127.0.0.1，解决 Windows 下，连接本地 redis 失败问题
        HostAndPort.setLocalhost(LOCALHOST);
        Set<HostAndPort> nodes = buildNodes(this.redisConfig.getNodes());
        GenericObjectPoolConfig<Object> poolConfig = new GenericObjectPoolConfig<>();
        poolConfig.setMaxWaitMillis(this.redisConfig.getTimeout());
        if (StringUtils.isEmpty(this.redisConfig.getPassword())) {
            return new QHJedisCluster(nodes, this.redisConfig.getTimeout(), poolConfig);
        }
        return new QHJedisCluster(nodes, this.redisConfig.getTimeout(), this.redisConfig.getTimeout(), this.redisConfig.getMaxAttempts(), this.redisConfig.getPassword(), poolConfig);
    }

    private Set<HostAndPort> buildNodes(String nodes) {
        if (StringUtils.isEmpty(nodes)) {
            throw new IllegalArgumentException("Redis Cluster Nodes is invalid.");
        }
        Set<HostAndPort> hostAndPortSet = Arrays.stream(nodes.split(","))
                .map(StringUtils::trimToEmpty)
                .filter(StringUtils::isNoneEmpty)
                .map(HostAndPort::parseString)
                .collect(Collectors.toSet());

        if (hostAndPortSet.isEmpty()) {
            throw new IllegalArgumentException("Redis Cluster Nodes is invalid.");
        }
        return hostAndPortSet;
    }

    public RedisConfig getRedisConfig() {
        return redisConfig;
    }

    public void setRedisConfig(RedisConfig redisConfig) {
        this.redisConfig = redisConfig;
    }
}
