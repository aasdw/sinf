package sinfo.util.cache;

public interface ICacheResolver<T> {

    T resolve() throws Exception;
}
