package sinfo.util.cache;

import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.Tuple;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface ICacheManager {
     JedisCluster getJedisCluster();

    Set<String> keys(String pattern);

    String get(String key);

    <T> T get(String key, Class<T> classOfType);

    <T> List<T> getList(String key, Class<T> classOfType);

    long getNumber(String key);

    void set(String key, Object value);

    void set(String key, Object value, int seconds);

    void setString(String key, String value);

    void setString(String key, String value, int seconds);

    long expire(String key, int seconds);

    void del(String key);

    long incrBy(String key, long delta);

    long decrBy(String key, long delta);

    Long hSet(String key, String filed, String value);

    Long hincrBy(String key, String filed, Long value);

    Map<String, String> hgetAll(String key);

    Set<String> hkeys(String key);

    List<String> hmget(String key, String... fields);

    long hdel(String key, String... fields);

    Tuple zPopMin(String key);

    long zAdd(String key, long score, String value);

    Double zScore(String key, String value);

    <T> T remember(String key, int seconds, Class<T> classOfType, ICacheResolver<T> resolver) throws Exception;

    <T> List<T> rememberList(String key, int seconds, Class<T> classOfType,ICacheResolver<List<T>> resolver) throws Exception;

    <T> T forever(String key, Class<T> classOfType, ICacheResolver<T> resolver) throws Exception;

    <T> List<T> foreverList(String key, Class<T> classOfType, ICacheResolver<List<T>> resolver) throws Exception;

    long timestamp();

    long bitcount(String key);

    boolean setbit(String key, long offset, String value);

    boolean getbit(String key, long offset);

    String setnx(String key, String value, int seconds);
}
