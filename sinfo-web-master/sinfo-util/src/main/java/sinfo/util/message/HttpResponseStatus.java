package sinfo.util.message;

public interface HttpResponseStatus {
    //成功
    int SUCCESS = 200;
    //错误
    int ERROR = 400;
    //错误，其他错误
    int OtherError = 2;
    //token验证失败
    int TOKEN_ERROR = 410;


}
