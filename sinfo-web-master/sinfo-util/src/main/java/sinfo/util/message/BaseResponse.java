package sinfo.util.message;

public class BaseResponse {
    /**
     * 状态码
     */
    private Integer code;
    /**
     * 成功状态
     */
    private boolean success;
    /**
     * 提示信息
     */
    private String message;

    private Object data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
