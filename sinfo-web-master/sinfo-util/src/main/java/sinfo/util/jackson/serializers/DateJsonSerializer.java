package sinfo.util.jackson.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.Date;

public class DateJsonSerializer extends JsonSerializer<Object> {

    @Override
    public void serialize(Object object, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        if (null == object) {
            jsonGenerator.writeNumber(0);
        } else {
            Date date = (Date) object;
            if (date.getTime() > 20000000000L) {
                jsonGenerator.writeNumber(date.getTime() / 1000);
            } else {
                jsonGenerator.writeNumber(date.getTime());
            }
        }
    }

}
