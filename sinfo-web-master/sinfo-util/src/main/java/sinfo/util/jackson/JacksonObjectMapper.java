package sinfo.util.jackson;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

public class JacksonObjectMapper extends ObjectMapper {

    public final static String DATE_FULL_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public JacksonObjectMapper() {
        super();
        this.setSerializationInclusion(JsonInclude.Include.ALWAYS);
        this.setDateFormat(new SimpleDateFormat(DATE_FULL_FORMAT));
        this.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        this.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        this.enable(JsonParser.Feature.ALLOW_SINGLE_QUOTES);
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.setSerializerModifier(new MySerializerModifier());
        simpleModule.addDeserializer(Date.class, new DateJsonDeserializer());
        simpleModule.addSerializer(LocalDateTime.class, new LocalDateTimeToLongSerializer());
        simpleModule.addDeserializer(LocalDateTime.class,new LocalDateTimeDeserializer());
        this.registerModule(simpleModule);
    }

}
