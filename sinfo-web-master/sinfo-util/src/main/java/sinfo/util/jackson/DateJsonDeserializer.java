package sinfo.util.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateJsonDeserializer extends JsonDeserializer<Date> {

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public Date deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonToken token = jsonParser.getCurrentToken();
        if (token == JsonToken.VALUE_STRING) {
            String value = jsonParser.getValueAsString();
            if (StringUtils.isNumeric(value)) {
                return parseDate(jsonParser);
            } else {
                try {
                    return dateFormat.parse(value);
                } catch (ParseException e) {
                    try {
                        return simpleDateFormat.parse(value);
                    } catch (ParseException parseException) {
                        return null;
                    }
                }
            }
        } else if (token == JsonToken.VALUE_NUMBER_INT) {
            return parseDate(jsonParser);
        }
        return null;
    }

    private Date parseDate(JsonParser jsonParser) throws IOException {
        long valueAsLong = jsonParser.getValueAsLong();
        if (valueAsLong < 2000000000L) {
            return new Date(valueAsLong * 1000);
        }
        return new Date(valueAsLong);
    }
}
