package sinfo.util.ssl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.*;

public class SSLTrustManager {
    private final static Logger logger = LoggerFactory.getLogger(SSLTrustManager.class);

    public static HostnameVerifier hostnameVerifier = (s, sslSession) -> true;

    public static void trustAllHttpsCertificates() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[1];
            TrustManager tm = new CustomTrustManager();
            trustAllCerts[0] = tm;
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, null);
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            logger.error("Https 请求异常", e);
        }
    }

    static class CustomTrustManager implements TrustManager, X509TrustManager {
        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            return null;
        }

        public boolean isServerTrusted(java.security.cert.X509Certificate[] certs) {
            return true;
        }

        public boolean isClientTrusted(java.security.cert.X509Certificate[] certs) {
            return true;
        }

        public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
        }

        public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
        }
    }
}
